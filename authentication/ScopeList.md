# Scope list

* GRAPHQL_ACFT - Aircraft see
* GRAPHQL_ACFT_EDIT - Aircraft edit
* GRAPHQL_ACTIVITY_HISTORY - Activity history
* GRAPHQL_AIRCRAFT_AVAILABILITY - Aircraft availability
* GRAPHQL_AIRCRAFT_INSURANCE_SEE - Aircraft insurance see
* GRAPHQL_AIRCRAFT_INSURANCE_EDIT - Aircraft insurance edit
* GRAPHQL_AIRCRAFT_DAY_NOTE_EDIT - Aircraft day note edit
* GRAPHQL_CREW_AVAILABILITY - Crew availability
* GRAPHQL_AIRCRAFT_ACTIVITIES - Aircraft activities
* GRAPHQL_AIRPORT - Airport
* GRAPHQL_AIRPORT_CATEGORY_SEE - Airport category see
* GRAPHQL_API_KEYS - API keys
* GRAPHQL_CHECKLIST_EDIT - Checklist edit
* GRAPHQL_CHECKLIST_OPS_SEE - Checklist OPS see
* GRAPHQL_CHECKLIST_OPS_EDIT - Checklist OPS edit
* GRAPHQL_CHECKLIST_SALES_SEE - Checklist SALES see
* GRAPHQL_CHECKLIST_SALES_EDIT - Checklist SALES edit
* GRAPHQL_CHECKLIST_CONFIGURATION_EDIT - Checklist configuration edit
* GRAPHQL_CONTACT - Contact see
* CONTACT_DELETE_PERMANENTLY - Permanently deleting contacts and anonymization of data
* GRAPHQL_CONTACT_EDIT - Contact edit
* CONTACT_MASK - Contact mask
* GRAPHQL_CREW_MEMBER - Crew Members
* GRAPHQL_CREW_MEMBER_EAPIS - eAPIS Master Crew List
* GRAPHQL_CREW_MEMBER_EDIT - Crew Members edit
* CREW_MEMBER_ENDORSEMENT_EDIT - Crew member personal endorsements edit
* CREW_MEMBER_EXTERNAL_ENDORSEMENT_EDIT - Crew member external endorsement edit
* CREW_MEMBER_ENDORSEMENT_SEE - Crew member personal endorsements see
* CREW_MEMBER_AIRPORT_ENDORSEMENT_EDIT - Crew member airport endorsements edit
* CREW_MEMBER_AIRPORT_ENDORSEMENT_SEE - Crew member airport endorsements see
* GRAPHQL_CREW_PANEL - Crew Calendar and Timeline
* GRAPHQL_CREW_CALENDAR - Crew Calendar
* GRAPHQL_CREW_TIMELINE - Crew Timeline
* GRAPHQL_DUTY_EDIT - Duty edit
* GRAPHQL_DUTY_SEE - Duty see
* GRAPHQL_DUTY_DETAILS - Duty details
* EMAIL_TEMPLATE_SEE - Email template see
* EMPTY_LEGS_SEE - Empty legs see
* ENDORSEMENT_DEFINITION_SEE - Endorsement definition see
* GRAPHQL_FEASIBILITY_CHECK - Feasibility check
* GRAPHQL_FLIGHT - Flight see
* GRAPHQL_FLIGHT_EDIT - Flight edit
* GRAPHQL_FLIGHT_PERMITS_EDIT - Flight permits edit
* GRAPHQL_FLIGHT_WATCH_EDIT - Flight Watch edit
* GRAPHQL_FLIGHT_WATCH - Flight Watch see
* GRAPHQL_FTL - FTL
* GRAPHQL_FUEL_SEE - Fuel see
* FUEL_EDIT - Fuel edit
* GRAPHQL_HANDLING_AGENT - Handling Agents
* INTEGRATION_SEE - Integration see
* INTEGRATION_FLIGHT_SUPPORT - Integration Flight Support
* GRAPHQL_JOURNEY_LOG - Journey Log
* GRAPHQL_JOURNEY_LOG_EDIT - Journey Log edit
* GRAPHQL_JOURNEY_LOG_FILE_SEE - Journey Log file see
* LANDING_PERMITS_SEE - Landing permits see
* LANDING_PERMITS_EDIT - Landing permits edit
* GRAPHQL_LOGIN - Login
* MAILBOX_CONFIG_EDIT - Mailbox config edit
* MAILBOX_CONFIG_SEE - Mailbox config see
* MAILBOX_EDIT - Mailbox edit
* MAILBOX_SEE - Mailbox see
* GRAPHQL_MAINTENANCE_SEE - Maintenance see
* GRAPHQL_MAINTENANCE_EDIT - Maintenance edit
* GRAPHQL_MX_HIL_EDIT - MX HIL edit
* GRAPHQL_MX_FLEET_DOCS - MX Fleet documents see
* GRAPHQL_MX_FLEET_DOCS_EDIT - MX Fleet documents edit
* GRAPHQL_OPERATOR - Operator
* OVERFLIGHT_PERMITS_SEE - Overflight permits see
* OVERFLIGHT_PERMITS_EDIT - Overflight permits edit
* GRAPHQL_OWNER_APP - OwnerApp edit
* GRAPHQL_OWNER_BOARD_SEE - Owner Board see
* GRAPHQL_OWNER_BOARD_CONFIG_SEE - Owner Board configuration see
* GRAPHQL_OWNER_BOARD_CONFIG_EDIT - Owner Board configuration edit
* GRAPHQL_PASSENGER_EDIT - PAX List edit
* GRAPHQL_PASSENGER - PAX List see
* GRAPHQL_PAX_BOARDING_EDIT - PAX boarding edit
* GRAPHQL_POSITIONING_EDIT - Positioning edit
* GRAPHQL_POSITIONING_SEE - Positioning see
* PRICE_LIST - Price list see
* REPORT_WIZARD_EDIT - Report wizard edit
* REPORT_WIZARD_SEE - Report wizard see
* GRAPHQL_RESERVATION_EDIT - Reservation edit
* GRAPHQL_RESERVATION_SEE - Reservation see
* GRAPHQL_SABRE_IMPORT - Sabre files import
* GRAPHQL_SALES_BOOKINGS - Bookings
* GRAPHQL_SALES_QUOTE_EDIT - Quote edit
* GRAPHQL_SALES_TRIP_SUMMARY_TAB_SEE - Quote - OPS & Crew tab
* GRAPHQL_SALES_QUOTE_REQUEST_SEE - Quote request see
* GRAPHQL_SALES_FEES_SEE - Sales - Fees see
* GRAPHQL_SALES_FEES_EDIT - Sales - Fees edit
* GRAPHQL_SCHEDULE_ORDER_EDIT - Sched edit
* GRAPHQL_SCHEDULE_ORDER_PUBLICATION - Sched publication
* GRAPHQL_SCHEDULE_ORDER_SEE - Sched see
* GRAPHQL_SIMULATOR_EDIT - Simulator edit
* GRAPHQL_SIMULATOR_SEE - Simulator see
* SSIM_EDIT - SSIM edit
* INTEGRATION_PNR_GO_SEE - Integration PNR GO see
* INTEGRATION_PNR_GO_EDIT - Integration PNR GO edit
* INTEGRATION_GOVLINK_SEE - Integration GOVlink see
* INTEGRATION_GOVLINK_EDIT - Integration GOVlink edit
* PHONEBOOK_DOCUMENTS_SEE - Phonebook documents see
* PHONEBOOK_DOCUMENTS_EDIT - Phonebook documents edit
* PHONEBOOK_CHARACTERISTIC_SEE - Phonebook characteristic see
* PHONEBOOK_CHARACTERISTIC_EDIT - Phonebook characteristic edit
* PHONEBOOK_FLIGHTS_SEE - Phonebook flights see
* PHONEBOOK_INVOICES_SEE - Phonebook invoices see
* PHONEBOOK_EXPORT_EDIT - Phonebook export
* LOGBOOK - Logbook
* GRAPHQL_DUTY_ACCOUNT_SEE - Duty account see
* GRAPHQL_DUTY_ACCOUNT_EDIT - Duty account edit
* GRAPHQL_ROSTER_CHANGES_STATUS_EDIT - Roster changes status edit
* GRAPHQL_UPLOAD_FUEL_RELEASE - Allow to upload files to checklist fuel release element. Add it if not present.
* GRAPHQL_PAIRING_SEE - Pairing see
* GRAPHQL_PAIRING_EDIT - Pairing edit
* GRAPHQL_PAIRING_CREW_EDIT - Pairing crew edit
* GRAPHQL_COUNTRY_DATABASE_SEE - Country database see
* GRAPHQL_COUNTRY_DATABASE_EDIT - Country database edit
* INTERACTIVE_API_SEE - Interactive API see
* INTERACTIVE_API_EDIT - Interactive API edit