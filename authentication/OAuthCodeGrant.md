# Authentication with OAuth2 code grant
This method allows for creating integrations that will be accessible to multiple operators and will be visible in Addons panel.

## Getting started
### 1. Register as the 3rd party application provider
As a 3rd party application provider, you will have to use OAuth Code Grant authorization method. To use OAuth2 Code Grant authentication method we must first register your application in Leon. 

To facilitate that please send a [registration form](https://leonsoftware.atlassian.net/servicedesk/customer/portal/4/group/8/create/40) containing following information:

* Application name
* Application type (is your application a marketplace, flight support service provider, EFB, SMS, pilot's logbook, or something else?)
* Redirect URI (URI to which Leon will redirect the browser after authentication, passing the authorization code)
* Description (a description of your application that will be displayed in the Addons panel)
* Phone number which will be used to secure your client secret
* Application / company logo
* Your contact information
* Company website URL

After registration, we will provide you with your *client ID* and *client secret* which will be necessary for the integration. At this point, you will be able to develop and test the integration using [sandbox.leon.aero](https://sandbox.leon.aero/) environment, but still unable to work on production. 

### 2. Develop and test the integration
Develop the integration, using the GraphQL API. The API allows you to fetch data from Leon, as well as write the data
to it. You can develop and test the integration using [sandbox.leon.aero](https://sandbox.leon.aero/) environment. 

### 3. Contact us, to be switched to production mode

After the development and testing is done, and the application is ready to be used on production, please contact [Customer Support](https://customer.leon.aero/) and ask for
a demo meeting of your integration. After the meeting, if your application meets the requirements, you will be switched to production mode.

### 4. Onboard your customers
Once switched to production mode, you will be able to connect your software with your customers using Leon.

## Authentication flow
After obtaining *client ID* and *client secret*, authentication with code grant can be performed using the following flow.

### 1. Requesting authorization code (authorization endpoint)
First step of authentication is to get user's consent for your app to access Leon on their behalf. 
This is accomplished by redirecting user to URI constructed as follows.

~~~~
{BASE_URI}/oauth2/code/authorize/?
    response_type=code
    &client_id={CLIENT_ID}
    &redirect_uri={REDIRECT_URI}
    &scope={SCOPE_LIST}
    &state={OPTIONAL_STATE}
~~~~

Where

* **BASE_URI** is Leon address including oprId (as described in [README](../README.md)), which must be provided by the user before generating redirection link.
    * https://**oprId**.leon.aero on production
    * https://**oprId**.sandbox.leon.aero on development
* **CLIENT_ID** is your client ID obtained after registration
* **REDIRECT_URI** is the URI user will be redirected to after providing consent, this must be the same as the redirect URI you provided during registration step
* **SCOPE** is space-delimited list of scopes you require access to (for the list of scopes please refer to [this page](ScopeList.md))
* **OPTIONAL_STATE** is and optional parameter, which can contain any value and will be passed on to your redirect URI after user provides consent
    * This can be used to protect against cross-site request forgery (CSRF) attacks by ensuring that the returned state matches your specified state

Once user grants consent to your application they will be redirected to redirect URI you provided with following data in query string:

* `code` is an authorization code required for the next step (single-use, valid for 10 minutes)
* `state` will be present if you provided any value as **OPTIONAL_STATE**

For most applications, only users with admin privileges are able to authorize 3rd party application in that flow. 
It doesn't apply to 'personal' applications, which can access only single user's details, like pilot logbook, etc.

### 2. Obtaining refresh and access token pair (token endpoint)
After receiving authorization code in previous step your application can now request refresh and access token from Leon.
These will provide access to Leon API with scopes requested in previous step.

To do this send the following POST request (mind the trailing slash!).

If you work on MS Windows, remember that Windows CMD does not support single quotes (`'`) for strings. You should replace them with double quotes (`"`).

~~~~
curl --location --request POST '{BASE_URI}/oauth2/code/token/' \
    --form 'grant_type="authorization_code"' \
    --form 'client_id="{CLIENT_ID}"' \
    --form 'client_secret="{CLIENT_SECRET}"' \
    --form 'redirect_uri="{REDIRECT_URI}"' \
    --form 'code="{AUTHORZIATION_CODE}"'
~~~~

Where

* **BASE_URI** is Leon address including operator ID (as described in [README](../README.md)), which must be provided by the user before generating redirection link.
    * https://**oprId**.leon.aero on production
    * https://**oprId**.sandbox.leon.aero on development
* **CLIENT_ID** is your client ID obtained after registration
* **CLIENT_SECRET** is your client secret obtained after registration
* **REDIRECT_URI** is the URI user will be redirected to after providing consent, this must be the same as the redirect URI you provided during registration step
* **AUTHORIZATION_CODE** is the code generated in previous step and passed as `code` parameter to redirect URI

In return to above request, provided everything is correct, response will contain the following JSON with token data.

~~~~
{
    "token_type": "Bearer",
    "expires_in": {EXPIRY_TIMESTAMP},
    "access_token": "{ACCESS_TOKEN}",
    "refresh_token": "{REFRESH_TOKEN}"
}
~~~~

Access token is short-lived. It expires after 30 minutes. During this time, you can use it to execute queries.

After access token expires you can request new one, using the below request:

~~~~
curl --location --request POST '{BASE_URI}/oauth2/code/token/' \
    --form 'grant_type="refresh_token"' \
    --form 'client_id="{CLIENT_ID}"' \
    --form 'client_secret="{CLIENT_SECRET}"' \
    --form 'refresh_token="{REFRESH_TOKEN}"'
~~~~

Refresh token is valid 30 days since its last use. 

### 3. Executing requests with access token
While access token is valid you can issue requests with it, example below. Only requests involving authorized scopes
will be accepted. If you are not sure which scopes will be required for your application, check [Documentation Portal](http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/).
Required scopes will be mentioned in descriptions of every node that checks the scopes.

~~~~
curl '{BASE_URI}/api/graphql/' 
    -H 'Content-Type: application/json' 
    -H 'Authorization: Bearer {ACCESS_TOKEN}' 
    --data-binary '{"query":"query {\n  aircraftTypeList(wildcard: \"CL30\") {\n    acftTypeId,\n    iCAO\n  }\n}"}'
~~~~

## Additional resources

1. [OAuth 2.0 Simplified - Example Flow](https://www.oauth.com/oauth2-servers/server-side-apps/example-flow/) - step-by-step example flow at OAuth.com
2. [Oauth 2.0 playground](https://www.oauth.com/playground/index.html) - OAuth Playground at OAuth.com