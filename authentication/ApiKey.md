# Authentication with API keys

This method can be used only in case of integrations created for single operator.
Additionally applications authenticated with API key will not be visible in Addons panel.

To authenticate with the API using this method, you will need to create an API key in Leon. 
To do that follow the steps in [Generating Refresh tokens](https://wiki.leonsoftware.com/leon/api-keys). The list of scopes used in the documentation, referred
to the resources in the API Keys interface can be found [here](ScopeList.md). 
Creating the API key will provide you with a _RefreshToken_, which you can use to generate Access tokens:

~~~~

curl -X POST -d 'refresh_token=_RefreshToken_' https://yourOprId.leon.aero/access_token/refresh/

c137be70d5at716cc8e8c9es5nfe20fce102b13dfefac9e60ad25edd92734bd6f1e1f3269

~~~~

If you work on MS Windows, remember that Windows CMD does not support single quotes (`'`) for strings. You should replace them with double quotes (`"`).

Access token is short-lived. It expires after 30 minutes. During this time, you can use it to execute queries:

~~~~
curl 'https://yourOprId.leon.aero/api/graphql/' -H 'Content-Type: application/json' -H 'Authorization: Bearer c137be70d5at716cc8e8c9es5nfe20fce102b13dfefac9e60ad25edd92734bd6f1e1f3269' --data-binary '{"query":"query {\n  aircraftTypeList(wildcard: \"CL30\") {\n    acftTypeId,\n    iCAO\n  }\n}"}'

{
  "data": {
    "aircraftTypeList": [
      {
        "acftTypeId": "CL30",
        "iCAO": "CL30"
      },
      {
        "acftTypeId": "BDC30",
        "iCAO": "CL30"
      },
      {
        "acftTypeId": "CL301",
        "iCAO": "CL30"
      }
    ]
  }
}

~~~~

[Convert curl syntax to Python, Ansible URI, Node.js, R, PHP, Strest, Go, Dart, JSON, Rust](https://curl.trillworks.com/#python)  
