# Leon integration for flight support services providers and suppliers

This type of integration workflow is dedicated to 3rd party companies providing services to air operators using Leon.

Integrating your flight support system with Leon allows you to easily exchange the data between your own system and
Leon, reducing the manual work to the absolute minimum, as well as eliminating e-mail messages from the cooperation
process.

## How it works?

### General description

The functionality of the API for flight support providers is shown in the diagram, which you can
find [here](TripSupportIntegrationDiagram.pdf). However, the diagram may not contain some of the objects available in the API. 

### Flight support service availability

By default, on development stage, you can have all or selected services on your list. All possibilities available can be found [here](http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/flightsupportserviceenum.doc.html). You can shorten or extend this list later. 
All the services selected there will be available by default at all locations. 

However, it's possible to set services availability per location, using the [setFlightSupportAvailableAirportServiceList](../../sample-queries/flight-support/mutation/setFlightSupportAvailableAirportServiceList.txt) mutation. In this case, you must define a list for every single airport you would like to provide your services at, selecting the services from the default list. Otherwise, the services will be available only at the locations for which the list is defined. 

### Sample queries 

Sample API queries for flight support providers, covering virtually all scenarios and use cases, can be found [here](https://bitbucket.org/leondevteam/api-documentation/src/master/sample-queries/flight-support/). All of them works with just one access scope
`INTEGRATION_FLIGHT_SUPPORT`.

### Leon user's point of view

The functionality of the integration from a Leon user's point of view is described in the Leon Software Wiki, where you can find the description of:

* [Integration configuration](https://wiki.leonsoftware.com/leon/integrations#flight-support-service-providers-and-suppliers)
* [Making a request](https://wiki.leonsoftware.com/leon/checklist#integrated-flight-service-providers-and-suppliers)

#### Service status vs. checklist item status

For each service, you can set a `FlightSupportServiceStatus` using the `setServiceStatus` mutation. The possible statuses for flight support services can be found [here](http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/flightsupportservicestatus.doc.html). 

Each service is associated with a corresponding checklist item. After setting a service status with the `setServiceStatus` mutation, the status of the corresponding checklist item will be updated according to the mapping provided in the table below:

```
+========================================================+===============================================================================================================================+
|                                |                       |                                            SERVICE STATUS -> CHECKLIST STATUS MAP                                             |
|     FLIGHT SUPPORT SERVICE     |    CHECKLIST ITEM     +=================+=================+=================+==================+==================+==============+====================+
|                                |                       |     PENDING     |     ACTIVE      |     BRIEFED     |    CONFIRMED     |     COMPLETED    |  CANCELLED   |    NOT_REQUIRED    |
+================================+=======================+=================+=================+=================+==================+==================+==============+====================+
|CATERING_ADEP                   |Catering               |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|CREW_TRANSPORT_ADEP             |Crew Transport (ADEP)  |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Yes (YES)         |Yes (YES)         |? (QSM)       |Not Applicable (NAP)|
|HANDLING_ADEP                   |Handling (ADEP)        |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|PAX_TRANSPORT_ADEP              |PAX transport (ADEP)   |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|PPR_ADEP                        |PPR (ADEP)             |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|SLOT_ADEP                       |Slot (ADEP)            |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|TAKE_OFF_PERMIT_ADEP            |Departure Permission   |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|VIP_LOUNGE_ADEP                 |VIP Lounge (ADEP)      |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|CREW_TRANSPORT_ADES             |Crew Transport (ADES)  |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Yes (YES)         |Yes (YES)         |? (QSM)       |Not Applicable (NAP)|
|HANDLING_ADES                   |Handling (ADES)        |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|PAX_TRANSPORT_ADES              |PAX transport (ADES)   |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|PPR_ADES                        |PPR (ADES)             |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|SLOT_ADES                       |Slot (ADES)            |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|VIP_LOUNGE_ADES                 |VIP Lounge (ADES)      |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|FUEL_ADEP                       |Fuel                   |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Yes (YES)         |Yes (YES)         |? (QSM)       |Not Applicable (NAP)|
|PRELIMINARY_FPL_PREPARATION_ADEP|Preliminary FPL        |In progress (PRS)|In progress (PRS)|In progress (PRS)|Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|FINAL_FPL_PREPARATION_ADEP      |Final FPL              |In progress (PRS)|In progress (PRS)|In progress (PRS)|Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|ATC_FPL_FILING_ADEP             |ATC flight plan        |OK (OKI)         |OK (OKI)         |OK (OKI)         |Acknowledged (ACK)|Acknowledged (ACK)|No (NOO)      |Not Applicable (NAP)|
|WEATHER_PACKAGE_ADEP            |Weather                |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Acknowledged (ACK)|Acknowledged (ACK)|? (QSM)       |Not Applicable (NAP)|
|JEPPESEN_CHARTS_ADEP            |Jeppesen               |Requested (RQS)  |Requested (RQS)  |Requested (RQS)  |Acknowledged (ACK)|Acknowledged (ACK)|? (QSM)       |Not Applicable (NAP)|
|AIRPORT_FEES_ADEP               |Airport Fees (ADEP)    |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|AIRPORT_FEES_ADES               |Airport Fees (ADES)    |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|TECHNICAL_LANDING_ADES          |Landing permit(s)      |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|HOTEL_ADES                      |Hotel                  |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|CREW_VISA_ADES                  |Crew Visa (ADES)       |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|API_ADEP                        |API (ADEP)             |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|API_ADES                        |API (ADES)             |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|PARKING_ADES                    |Parking                |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|SECURITY_ADEP                   |Security (ADEP)        |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|SECURITY_ADES                   |Security (ADES)        |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|SUPERVISION_ADEP                |Supervision (ADEP)     |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|SUPERVISION_ADES                |Supervision (ADES)     |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|EQUIPMENT_ADEP                  |Equipment (ADEP)       |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|EQUIPMENT_ADES                  |Equipment (ADES)       |In progress (PRS)|In progress (PRS)|Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|STANDARD_BRIEFING_ADEP          |Standard briefing      |In progress (PRS)|In progress (PRS)|Completed (COM)  |Completed (COM)   |Completed (COM)   |Rejected (REJ)|Not Applicable (NAP)|
|STANDARD_EROPS_BRIEFING_ADEP    |Standard EROPS briefing|In progress (PRS)|In progress (PRS)|Completed (COM)  |Completed (COM)   |Completed (COM)   |Rejected (REJ)|Not Applicable (NAP)|
|EAPIS_ADEP                      |eAPIS (ADEP)           |Yes (YES)        |Yes (YES)        |Yes (YES)        |Yes (YES)         |Yes (YES)         |? (QSM)       |Not Applicable (NAP)|
|EAPIS_ADES                      |eAPIS (ADES)           |Yes (YES)        |Yes (YES)        |Yes (YES)        |Yes (YES)         |Yes (YES)         |? (QSM)       |Not Applicable (NAP)|
|CBP_US_ADEP                     |CBP US (ADEP)          |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|CBP_US_ADES                     |CBP US (ADES)          |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|CATERING_ADES                   |Catering (ADES)        |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|HANGAR_ADEP                     |Hangar(ADEP)           |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
|HANGAR_ADES                     |Hangar(ADES)           |Requested (RQS)  |Requested (RQS)  |Confirmed (CNF)  |Confirmed (CNF)   |Confirmed (CNF)   |Rejected (REJ)|Not Applicable (NAP)|
+--------------------------------+-----------------------+-----------------+-----------------+-----------------+------------------+------------------+--------------+--------------------+
```

If the mapping for any checklist item is not set as described above, or if the checklist status that should be set is not found (e.g., it has been disabled by an operator in the checklist configuration panel), a default mapping will be applied based on the "best match" rule. This rule will be applied in the following order for each service status:

```
+====================+=============================+   +====================+==========================+   +====================+==========================+
|   SERVICE STATUS   |      CHECKLIST STATUS       |   |   SERVICE STATUS   |     CHECKLIST STATUS     |   |   SERVICE STATUS   |     CHECKLIST STATUS     |
+====================+=============================+   +====================+==========================+   +====================+==========================+
|    NOT_REQUIRED    | 1. Not applicable (NAP)     |   |     CANCELLED      | 1. Rejected (REJ)        |   |      BRIEFED       | 1. Pending (PND)         |
|                    |                             |   |                    | 2. No (YES)              |   |                    | 2. In progress (PRS)     |
|                    |                             |   |                    |                          |   |                    | 3. Requested (RQS)       |
|                    |                             |   |                    |                          |   |                    | 4. OK (OKI)              |
+--------------------+-----------------------------+   +--------------------+--------------------------+   +--------------------+--------------------------+
|     PENDING        | 1. Pending (PND)            |   |      ACTIVE        | 1. In progress (PRS)     |   |    CONFIRMED       | 1. Confirmed (CNF)       |
|                    | 2. In progress (PRS)        |   |                    | 2. Pending (PND)         |   |                    | 2. Yes (YES)             |
|                    | 3. Requested (RQS)          |   |                    | 3. Requested (RQS)       |   |                    | 3. Acknowledged (ACK)    |
|                    | 4. OK (OKI)                 |   |                    | 4. OK (OKI)              |   |                    |                          |
+--------------------+-----------------------------+   +--------------------+--------------------------+   +--------------------+--------------------------+
|    COMPLETED       | 1. Completed (COM)          |   |                    |                          |   |                    |                          |
|                    | 2. Confirmed (CNF)          |   |                    |                          |   |                    |                          |
|                    | 3. Yes (YES)                |   |                    |                          |   |                    |                          |
|                    | 4. Acknowledged (ACK)       |   |                    |                          |   |                    |                          |
+--------------------+-----------------------------+   +--------------------+--------------------------+   +--------------------+--------------------------+
```

If no best match is found among the operator's statuses for the checklist item, the `? (QSM)` — question mark status will be set instead.

## Further questions?

If you encounter any issues and cannot find the solution in the documentation, please contact us via the [Customer Portal](https://customer.leon.aero/).