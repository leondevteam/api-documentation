# Subscription notifications via webhooks
This subscription method will push notifications about subscribed events to a specified URL. 

#### Webhook creation
In order to start subscription that will use this notification method you have to save necessary webhook data using a mutation in our API. 
Mutations for creating and deleting this data can be found in [our documentation portal](http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/webhookmutationsection.doc.html).

To register webhook you need to provide following data.

* **label** is a unique identifier for registered webhook (must be more than 5 characters),
* **refreshToken** is a refresh token that will be used for resolving subscriptions,
* **webhookUrl** is URL address that will be called with subscription data when subscribed event occurs,
* **subscription** is a string containing a valid GraphQL subscription,
* **variables** is a JSON with variables for subscription.

You can have at most 10 webhooks registered at any given time.

You can find an example of mutation creating a new webhook below. 
~~~~

mutation {
  webhook {
    createSubscriptionWebhook(
      refreshToken: "<refreshToken>",
      label: "TEST1",
      subscription: "subscription Sub($txt: String!) { echo(text: $txt) }",
      variables: "{\"txt\": \"ABC\"}",
      webhookUrl: "<url>"
    ) {
      ... on CreateSubscriptionWebhookViolationList {
        error: value {
          message
          path
        }
      }
      ... on NonNullBooleanValue {
        result: value
      }
    }
  }
}

~~~~

Above will subscribe to a test `echo` subscription. You can test it by invoking a mutation that will push an event triggering this subscription.
Upon invoking this mutation your webhook should be triggered with "ABC111" as payload.
~~~~

mutation {
  echoSubscription(text: "111")
}

~~~~

#### Webhook deletion
In order to delete an existing webhook you can use an `deleteSubscriptionWebhook` mutation described in can be found in [our documentation portal](http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/webhookmutationsection.doc.html).
~~~~

mutation {
  webhook {
    deleteSubscriptionWebhook(label: "TEST1")
  }
}

~~~~

#### Notification format and authentication
Notifications from Leon will be sent as POST requests to URL specified during webhook creation. 
Notification data is dependent on registered GraphQL subscription and will be contained as JSON in request body.

Valid notifications from Leon will also include an `Authorization` header containing JWT token that should be a decrypted and validated to ensure security.
JWT in this header will contain following data.

* **iat** is a timestamp at which token was issued,
* **exp** is a token expiry timestamp,
* **jti** is an unique token identifier,
* **iss** (token issuer) must be equal to _Leon Software_,
* **aud** (token audience) must be equal to URL specified during webhook creation.

Authorization token is signed using RS512 algorithm. To verify the signature you can use a public key available under following URL `https://{oprId}.leon.aero/.well-known/keys/leon-subscriptions-webhook-1.pub` where `oprId` is your operator code.

You can find more information about JWT tokens as well as a list of libraries you can use to handle JWT token authorization [here](https://jwt.io/libraries).

#### Available subscriptions
You can find a list of available subscriptions in our [documentation portal](http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/subscription.doc.html).