# Leon API Documentation

### About GraphQL

The [GraphQL](https://graphql.org/) is a query language for API originally developed by Facebook. It became an API standard for modern applications. It is strongly typed, hierarchical and self-documenting.

### Why does Leon use GraphQL

Leon chose GraphQL because of its flexibility for our integrators. The ability to define precisely the data you want to fetch is a great advantage over the REST API endpoints. GraphQL lets you replace multiple REST requests with a single call to fetch the exact data you specify.

### Schema
1. [Documentation Portal](http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/)
2. [JSON Schema](http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/schema.json)

### Endpoints

#### Production
 
https://**yourOprId**.leon.aero/api/graphql/
 
#### Development / Sandbox
 
It contains a copy of production data, and it is refreshed once a day (03:00 UTC)

https://**yourOprId**.sandbox.leon.aero/api/graphql/

#### What is **yourOprId**?

Each operator in Leon is given dedicated domain which is used to sign in to the system. For example our demo operator is given **demo**.leon.aero domain. The **yourOprId** is the first segment of the domain. So in case of demo operator oprId is **demo**. If you are not sure what is yourOprId just ask any user what www address it uses to sign in to the Leon.


### Authentication
You can authenticate with Leon API in two different ways.

#### [Using the manually created API key](authentication/ApiKey.md)
This method can be used only in case of integrations **created for single operator**. 
Additionally applications authenticated with API key **will not be visible in Addons** panel.

Learn more about this method [here](authentication/ApiKey.md)

#### [Using OAuth code grant](authentication/OAuthCodeGrant.md)
This method allows for creating integrations that will be accessible to **multiple operators** and will **be visible in Addons** panel. This method is **mandatory for 3rd party software providers**. 

Learn more about this method [here](authentication/OAuthCodeGrant.md)

### [Subscriptions (webhooks)](subscriptions/Webhook.md)
You can subscribe to certain events in Leon using [GraphQL subscriptions](https://graphql.org/blog/subscriptions-in-graphql-and-relay/). 
Available subscriptions can be found in our [documentation portal](http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/subscription.doc.html).

Learn more [here](subscriptions/Webhook.md).

### Standard workflow use cases

#### [Flight Support](standard-workflows/flight-support/FlightSupport.md)
If you provide flight support services and you would like to integrate with Leon, learn more [here](standard-workflows/flight-support/FlightSupport.md).

### Checking your code validity against Leon's GraphQL schema
A new application version is released every two weeks. Each change can contain breaking changes which can make your application fail. It is recommended to establish an automated validation of your code against our staging GraphQL schema. 

The staging GraphQL schema can be found at http://api-schema-doc.s3-website-eu-west-1.amazonaws.com/schema-staging.json 

The schema is generated daily after 07:00 UTC, and will be applied on production during the rolling process, after the sprint is finished. 

For an example on how to do it using TypeScript, click [here](validation-example/TypeScript.md). 

### Client libs and tools

There are dozens of GraphQL client libs for different programming languages. Some of them can be found at [GraphQL Code](https://graphql.org/code/). GraphQL is also famous of great tooling:

1. [10+ Awesome Tools and Extensions For GraphQL APIs](https://nordicapis.com/10-awesome-tools-and-extensions-for-graphql-apis/)
2. [13 GraphQL Tools and Libraries You Should Know in 2019](https://blog.bitsrc.io/13-graphql-tools-and-libraries-you-should-know-in-2019-e4b9005f6fc2)
3. [TOP 10 TOOLS & EXTENSIONS TO PIMP YOUR GRAPHQL APIS](https://divante.co/blog/top-10-tools-extensions-pimp-graphql-apis/)

We actually use some of them here at Leon, and we encourage you to try using [Prisma Playground](https://github.com/prisma/graphql-playground) which is IDE for GraphQL. It is the most convenient way to connect to our API. 

Another tool we can recommend is [Altair GraphQL Client](https://altairgraphql.dev/). 

### FAQ

*I am application developer and I want to get it integrated with Leon. What should I do?*

>If you are a 3rd party application developer, please follow the steps described on the page [OAuth Code Grant](authentication/OAuthCodeGrant.md). This method is mandatory for 3rd party applications. 

>If you are Leon customer developing an app for your internal use, please use [manually created API Key](authentication/ApiKey.md).

*Data I am looking for are missing. Can you add them to the schema?*

>Yes, we can! The API is very new so not all data have been already shared. Please send an email to support@leonsoftware.com letting us know what queries or mutations you need.

*What is myOprId?*

>Each operator in Leon is given dedicated domain which is used to sign in to the system. For example our demo operator is given **demo**.leon.aero domain. The oprId is the first segment of the domain. So in case on demo operator oprId is **demo**  

*Where can I find sample queries*

>Please visit [sample queries directory](https://bitbucket.org/leondevteam/api-documentation/src/master/sample-queries/)
