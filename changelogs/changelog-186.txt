Detected the following changes (110) between schemas:

✖  Argument aircraftTypeList: [AircraftTypeScalar!] was removed from field FleetQuerySection.getAircraftList
✖  Argument showCancelled: Boolean! added to field FlightsQuery.getFlightChecklistItemDueDateList
✖  Field blockHour was removed from object type MySchedule
✖  Field MySchedule.history changed type from [LoggedUserHistoryType!]! to LoggedUserHistoryType!
✖  Input field doctor was added to input object type QuoteRealizationSubcharterInput
✖  Input field paramedic was added to input object type QuoteRealizationSubcharterInput
✖  Field replyToAvinodeSubcharterOperator was removed from object type SubcharterMutation
✖  Field replyToMarketplaceSubcharterOperator was removed from object type SubcharterMutation
⚠  Input field crewNote was added to input object type CrewMemberEndorsementInputType
⚠  Input field crewNote was added to input object type CrewMemberExternalEndorsementInputType
⚠  Input field crewNote was added to input object type CrewMemberMobileEndorsementInputType
⚠  Enum value dot_efvs_night_pf was added to enum CurrencyIdEnum
⚠  Enum value dot_efvs_night_pm was added to enum CurrencyIdEnum
⚠  Enum value dot_efvsa_pf was added to enum CurrencyIdEnum
⚠  Enum value dot_efvsa_pm was added to enum CurrencyIdEnum
⚠  Enum value dot_efvsl_pf was added to enum CurrencyIdEnum
⚠  Enum value dot_efvsl_pm was added to enum CurrencyIdEnum
⚠  Enum value dot_ils_cat_23_autoland_pf was added to enum CurrencyIdEnum
⚠  Enum value dot_ils_cat_23_autoland_pm was added to enum CurrencyIdEnum
⚠  Enum value dot_ils_cat_23_hud_pf was added to enum CurrencyIdEnum
⚠  Enum value dot_ils_cat_23_hud_pm was added to enum CurrencyIdEnum
⚠  Enum value dot_ils_cat_2_pf was added to enum CurrencyIdEnum
⚠  Enum value dot_ils_cat_2_pm was added to enum CurrencyIdEnum
⚠  Argument aircraftNidList: [AircraftNid!] added to field FleetQuerySection.getAircraftList
⚠  Input field distance was added to input object type FlightCreate
⚠  Input field distance was added to input object type FlightUpdate
⚠  Input field distance was added to input object type FlightUpdateInput
⚠  Input field additional was added to input object type FuelAmountUpdateInput
⚠  Input field alternate was added to input object type FuelAmountUpdateInput
⚠  Input field discretionary was added to input object type FuelAmountUpdateInput
⚠  Input field extra was added to input object type FuelAmountUpdateInput
⚠  Input field finalReserve was added to input object type FuelAmountUpdateInput
⚠  Enum value SAF_FUEL_BLEND was added to enum LegFuelFieldEnum
⚠  Enum value SAF_USED was added to enum LegFuelFieldEnum
⚠  Enum value GRAPHQL_AIRPORT_BRIEF_EDIT was added to enum PermissionResource
⚠  Enum value GRAPHQL_CHECKLIST_ITEM_AIRPORT_BRIEF_EDIT was added to enum PermissionResource
⚠  Enum value GRAPHQL_CHECKLIST_ITEM_AIRPORT_BRIEF_SEE was added to enum PermissionResource
⚠  Enum value GRAPHQL_SECURITY_SEE was added to enum PermissionResource
✔  Field color was added to object type AircraftMaintenance
✔  Field endTimeBase was added to object type AircraftMaintenance
✔  Field startTimeBase was added to object type AircraftMaintenance
✔  Type AirportDirectoryAirportBriefMutation was added
✔  Field airportBrief was added to object type AirportDirectoryMutation
✔  Field isEditableOnMyScheduleCrewApp was added to object type ChecklistDefType
✔  Field checklistStatusId was added to object type ChecklistStatusType
✔  Field isEditableOnMyScheduleCrewApp was added to object type CrewAppChecklistDefType
✔  Type CrewAppChecklistMutation was added
✔  Field CrewAppChecklistQuery.getFlightOpsChecklistItems description changed from All flight and trip checklist OPS items.
Requires access to resource GRAPHQL_CHECKLIST_OPS_SEE.
Requires access to given flight to All flight and trip checklist OPS items.
Requires access to given flight
✔  Field CrewAppChecklistQuery.getFlightSalesChecklistItems description changed from All flight and trip checklist SALES items.
Requires access to resource GRAPHQL_CHECKLIST_SALES_SEE.
Requires access to given flight to All flight and trip checklist SALES items.
Requires access to given flight
✔  Field safFuelBlend was added to object type CrewAppFuelChecklistItem
✔  Field safUsed was added to object type CrewAppFuelChecklistItem
✔  Field checklist was added to object type CrewAppMutation
✔  Field crewCode was added to object type CrewAppScheduleDuty
✔  Field midnightPassList was added to object type CrewPanelActivities
✔  Type CrewPanelMidnightPass was added
✔  Field east_west_long_distance_flights_calculate_min_rest was added to object type FtlSettings
✔  Field fdp_extended_count_condition was added to object type FtlSettings
✔  Field min_rest_after_fdp_extended_count was added to object type FtlSettings
✔  Field FtlSettings.reduce_max_fdp_due_to_time_zone_difference description changed from If this option is set, and the time zone difference between the current location and the home base (time zone offset) is 3 hours or greater, the max FDP shall be limited to 11:30. For each sector starting from the second, this limit shall be further reduced by 00:30. Also, FDP extensions cannot be used on such flights. to If this option is set, and the time zone difference between the current location and the home base (time zone offset) is 3 hours or greater, the max FDP shall be limited to 11:30. For each sector starting from sector in setting <em>$reduce_max_fdp_due_to_time_zone_difference_start_reduce_sector</em>, this limit shall be further reduced by 00:30. Also, FDP extensions cannot be used on such flights, unless <em>reduce_max_fdp_due_to_time_zone_difference_with_fdp_extension</em> is set.
✔  Field reduce_max_fdp_due_to_time_zone_difference_start_reduce_sector was added to object type FtlSettings
✔  Field reduce_max_fdp_due_to_time_zone_difference_with_fdp_extension was added to object type FtlSettings
✔  Field FtlSettings.rest_reduction_home_ambulance_times description changed from Lenght of the reduction, depending on the length of the consecutive rest after duty. Each row has format HH:MM hh:mm, and means rest reduction before duty with next rest length equal to or longer than HH:MM is hh:mm. If no exact matching entry is found, no rest reduction will be applied. to Length of the reduction, depending on the length of block time and the length of consecutive rest after duty. Each row has format H1:M1 H2:M2 hh:mm, and means rest reduction before duty with block time equal or longer than H1:M1 and next rest length equal to or longer than H2:M2 is hh:mm. If no exact matching entry is found, no rest reduction will be applied.
✔  Field FtlSettings.rest_reduction_home_times description changed from Lenght of the reduction, depending on the length of the consecutive rest after duty. Each row has format HH:MM hh:mm, and means rest reduction before duty with next rest length equal to or longer than HH:MM is hh:mm. If no exact matching entry is found, no rest reduction will be applied. to Length of the reduction, depending on the length of block time and the length of consecutive rest after duty. Each row has format H1:M1 H2:M2 hh:mm, and means rest reduction before duty with block time equal or longer than H1:M1 and next rest length equal to or longer than H2:M2 is hh:mm. If no exact matching entry is found, no rest reduction will be applied.
✔  Field FtlSettings.rest_reduction_on_commander_discretion_times description changed from Lenght of the reduction, depending on the length of the consecutive rest after duty. Each row has format HH:MM hh:mm, and means rest reduction before duty with next rest length equal to or longer than HH:MM is hh:mm. If no exact matching entry is found, no rest reduction will be applied. to Length of the reduction, depending on the length of block time and the length of consecutive rest after duty. Each row has format H1:M1 H2:M2 hh:mm, and means rest reduction before duty with block time equal or longer than H1:M1 and next rest length equal to or longer than H2:M2 is hh:mm. If no exact matching entry is found, no rest reduction will be applied.
✔  Field FtlSettings.rest_reduction_outside_ambulance_times description changed from Lenght of the reduction, depending on the length of the consecutive rest after duty. Each row has format HH:MM hh:mm, and means rest reduction before duty with next rest length equal to or longer than HH:MM is hh:mm. If no exact matching entry is found, no rest reduction will be applied. to Length of the reduction, depending on the length of block time and the length of consecutive rest after duty. Each row has format H1:M1 H2:M2 hh:mm, and means rest reduction before duty with block time equal or longer than H1:M1 and next rest length equal to or longer than H2:M2 is hh:mm. If no exact matching entry is found, no rest reduction will be applied.
✔  Field FtlSettings.rest_reduction_outside_times description changed from Lenght of the reduction, depending on the length of the consecutive rest after duty. Each row has format HH:MM hh:mm, and means rest reduction before duty with next rest length equal to or longer than HH:MM is hh:mm. If no exact matching entry is found, no rest reduction will be applied. to Length of the reduction, depending on the length of block time and the length of consecutive rest after duty. Each row has format H1:M1 H2:M2 hh:mm, and means rest reduction before duty with block time equal or longer than H1:M1 and next rest length equal to or longer than H2:M2 is hh:mm. If no exact matching entry is found, no rest reduction will be applied.
✔  Field additional was added to object type FuelAmount
✔  Field alternate was added to object type FuelAmount
✔  Field discretionary was added to object type FuelAmount
✔  Field extra was added to object type FuelAmount
✔  Field finalReserve was added to object type FuelAmount
✔  Field safFuelBlend was added to object type FuelChecklistItem
✔  Field safUsed was added to object type FuelChecklistItem
✔  Type IntegrationChecklistMutation was added
✔  Type IntegrationChecklistMutationChangeAirportBriefStatusOutput was added
✔  Type IntegrationChecklistMutationChangeAirportBriefStatusViolation was added
✔  Type IntegrationChecklistMutationChangeAirportBriefStatusViolationEnum was added
✔  Type IntegrationChecklistMutationChangeAirportBriefStatusViolationList was added
✔  Type IntegrationChecklistQuery was added
✔  Field checklist was added to object type IntegrationCommonMutation
✔  Field checklist was added to object type IntegrationCommonQuery
✔  Type LoginHistoryType was added
✔  Field crewNote was added to object type MyPersonalEndorsement
✔  Field blockHours was added to object type MySchedule
✔  Field isNoAccess was added to object type Operator
✔  Field isToDelete was added to object type Operator
✔  Field Operator.noAccess is deprecated
✔  Field Operator.noAccess has deprecation reason Use isNoAccess instead
✔  Field Operator.toDelete is deprecated
✔  Field Operator.toDelete has deprecation reason Use isToDelete instead
✔  Field setChecklistItemEditableOnMyScheduleCrewApp was added to object type OperatorChecklistSettingsMutation
✔  Field crewNote was added to object type PersonalEndorsement
✔  Field previousExpiryDate was added to object type PersonalEndorsementExpiring
✔  Field security was added to object type Query
✔  Field status was added to object type QuoteRealization
✔  Field doctor was added to object type QuoteRealizationSubcharter
✔  Field paramedic was added to object type QuoteRealizationSubcharter
✔  Type QuoteRequestMessageCount was added
✔  Field messageCountForUser was added to object type QuoteRequestSubcharterRequest
✔  Field QuoteRequestSubcharterRequest.unreadMessageCountForUser is deprecated
✔  Field QuoteRequestSubcharterRequest.unreadMessageCountForUser has deprecation reason Please use messageCountForUser field instead
✔  Field requestNid was added to object type QuoteRequesterListStatsRow
✔  Field subcharterRequest was added to object type SalesMutation
✔  Field subcharterRequest was added to object type SalesQuery
✔  Type SecurityQuery was added
✔  Field doctor was added to object type SubcharterModified
✔  Field paramedic was added to object type SubcharterModified
✔  Type SubcharterRequestMutation was added
✔  Type SubcharterRequestNid was added
✔  Type SubcharterRequestQuery was added
Detected 8 breaking changes
