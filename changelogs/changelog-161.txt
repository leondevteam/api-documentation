Detected the following changes (68) between schemas:

✖  Field AirportDetails.customs changed type from Int to Boolean
✖  Field AirportDetails.daylightSavings changed type from Int to Boolean
✖  Field AirportDetails.designatedInternational changed type from Int to Boolean
✖  Field AirportDetails.edto changed type from Int! to Boolean!
✖  Field AirportDetails.freeAirport changed type from Int to Boolean
✖  Field AirportDetails.handlingMandatory changed type from Int to Boolean
✖  Field AirportDetails.ilsDme changed type from Int to Boolean
✖  Field AirportDetails.landingRights changed type from Int to Boolean
✖  Field AirportDetails.slotsRequired changed type from Int to Boolean
✖  Field AirportDetails.userFees changed type from Int to Boolean
✖  Field AirportDetails.vor changed type from Int to Boolean
✖  Type AirportEndorsementIssueStatus was removed
✖  Argument countryDirectForbiddenList: [CountryCode!]! added to field CountryDirectoryMutation.settings
✖  Field CrewAppScheduleDuty.airport changed type from SimpleAirportType! to SimpleAirportType
✖  Field CrewAppScheduleDuty.endTimeLocal changed type from DateTimeWithTimezone! to DateTimeWithTimezone
✖  Field CrewAppScheduleDuty.startTimeLocal changed type from DateTimeWithTimezone! to DateTimeWithTimezone
✖  Input field CrewMemberAirportEndorsementFilterInput.issueStatus changed type from AirportEndorsementIssueStatus to EndorsementIssueStatus
✖  Field non_fdp_duties_count_as_non_acclimated was removed from object type FtlSettings
⚠  Enum value CATERING was added to enum ChecklistItemRule
⚠  Enum value PAX_TRANSPORT_ADEP was added to enum ChecklistItemRule
⚠  Enum value PAX_TRANSPORT_ADES was added to enum ChecklistItemRule
⚠  Enum value SLOT_ADEP was added to enum ChecklistItemRule
⚠  Enum value SLOT_ADES was added to enum ChecklistItemRule
⚠  Input field issueStatus was added to input object type CrewMemberEndorsementFilterInput
⚠  Input field picTime was added to input object type CrewMemberLoginExperienceInput
⚠  Input field defaultByCountryList was added to input object type NationalIdInput
⚠  Enum value CONTACT_MASK was added to enum PermissionResource
⚠  Input field defaultByCountryList was added to input object type PhonebookPassportCreateInput
✔  Type AircraftRegistrationOrVirtualAircraftName was added
✔  Type ContactMaskMutationSection was added
✔  Field mask was added to object type ContactMutation
✔  Field nationalIdByNid was added to object type ContactQuery
✔  Field passportByNid was added to object type ContactQuery
✔  Type ContactRecommendedTravelDocuments was added
✔  Field countryDirectForbiddenList was added to object type CountrySetting
✔  Field changesHistory was added to object type CrewMemberOnLeg
✔  Type DefaultPassengerData was added
✔  Type DefaultPassengerDataQuery was added
✔  Type DefaultTravelDocumentCountryCriteria was added
✔  Type DefaultTravelDocumentCountryCriteriaInput was added
✔  Field canBeRequested was added to object type DutyDefinition
✔  Type EndorsementIssueStatus was added
✔  Field flightListOnWhichModifiedCrew was added to object type FlightSupportQuery
✔  Field flightListOnWhichModifiedPassengerList was added to object type FlightSupportQuery
✔  Field flightCrewChanged was added to object type FlightSupportSubscriptionSection
✔  Field flightPassengerListChanged was added to object type FlightSupportSubscriptionSection
✔  Field non_fdp_duties_preserve_non_acclimated_count was added to object type FtlSettings
✔  Type HistoryActivityCrewReportingTime was added
✔  Field schedSelectedActivitiesVersionSsim was added to object type IataMessagesQuery
✔  Type IntegrationCommonMutation was added
✔  Field common was added to object type IntegrationMutation
✔  Type IntegrationSchedMutation was added
✔  Type IntegrationSchedMutationCreateSchedFlightListOutput was added
✔  Type IntegrationSchedMutationCreateSchedFlightListViolation was added
✔  Type IntegrationSchedMutationCreateSchedFlightListViolationEnum was added
✔  Type IntegrationSchedMutationCreateSchedFlightListViolationList was added
✔  Type IntegrationSchedUpdate was added
✔  Field picTime was added to object type LoginExperience
✔  Field defaultByCountryList was added to object type NationalId
✔  Type NonNullIntegrationSchedUpdateValue was added
✔  Type PassengerQuery was added
✔  Field defaultByCountryList was added to object type PassportOutput
✔  Field isMasked was added to object type PassportOutput
✔  Field passenger was added to object type Query
✔  Type ReportingHistorySnapshot was added
✔  Type SchedSpecifiedFlightInput was added
✔  Type SchedVersionNameOption was added
✔  Field schedSelectedActivitiesVersionExcel was added to object type ScheduleQuery
Detected 18 breaking changes
