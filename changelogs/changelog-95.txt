Detected the following changes (55) between schemas:

✖  Field allowedAirports (deprecated) was removed from object type AircraftInsuranceExcludedCountry
✖  Input field allowedAirports was removed from input object type AircraftInsuranceExcludedCountryInput
✖  Argument scheduleGroupNid: ScheduleGroupNid! added to field IataMessagesMutation.uploadSsimFile
✖  Field findSsimUploadedFlights was removed from object type IataMessagesQuery
✖  Field QuoteRealizationSubcharter.subcharterGroup changed type from QuoteRequestSubcharterGroup to QuoteRequestSubcharterRequest
✖  Type QuoteRequestSubcharterGroup was removed
✖  Field findAircraft was removed from object type SalesQuery
✖  Type SsimUploadedFlight was removed
⚠  Input field towbarOnBoard was added to input object type AircraftCreate
⚠  Input field towbarOnBoard was added to input object type AircraftUpdate
✔  Type AcftTowbarOnBoardEnum was added
✔  Field towbarOnBoard was added to object type Aircraft
✔  Field pilotNote was added to object type CrewPanelFlight
✔  Field plannerNote was added to object type CrewPanelFlight
✔  Field category was added to object type FeasibilityCheckInfo
✔  Type FlightOrQuoteRealizationLeg was added
✔  Field flightWatchCorrelation was added to object type FlightWatch
✔  Field flightWatchNid was added to object type FlightWatch
✔  Type FlightWatchChanges was added
✔  Field getModifiedFlightWatchList was added to object type FlightWatchQuery
✔  Field discretion_max_per_time was added to object type FtlSettings
✔  Field duty_after_last_positioning_commercial_carrier was added to object type FtlSettings
✔  Field extensionDueDate was added to object type Hil
✔  Type InvoiceDueDateType was added
✔  Type MarketplaceEnum was added
✔  Type NonNullFlightValue was added
✔  Type NonNullQuoteRealizationLegValue was added
✔  Type NonNullQuoteRequestValue was added
✔  Type NonNullQuoteRequestValueOrErrorList was added
✔  Field dueDateDays was added to object type QuoteDefaultInvoice
✔  Field dueDateType was added to object type QuoteDefaultInvoice
✔  Field QuoteRealization.legs description changed from ' to Requires access to resource GRAPHQL_SALES_QUOTE_REQUEST_SEE'
✔  Field acceptWebhook was added to object type QuoteRequest
✔  Field rejectWebhook was added to object type QuoteRequest
✔  Type QuoteRequestSubcharterRequest was added
✔  Type QuoteRequestWithWebhook was added
✔  Type QuoteRequestWithWebhookRepresentative was added
✔  Type QuoteRequestWithWebhookRequester was added
✔  Field getRouteCountries was added to object type RouteFinderQuery
✔  Field feasibilityCheck was added to object type SalesFindAircraftResult
✔  Field acceptWebhookRealizations was added to object type SalesMutation
✔  Field createRequestWithWebhook was added to object type SalesMutation
✔  Field rejectWebhookRealizations was added to object type SalesMutation
✔  Field findAircraftInit was added to object type SalesQuery
✔  Field findAircraftToken was added to object type SalesQuery
✔  Type SchedFlight was added
✔  Type SchedFlightNid was added
✔  Type ScheduleGroup was added
✔  Type ScheduleGroupNid was added
✔  Field createScheduleGroup was added to object type ScheduleMutation
✔  Field findSchedFlightList was added to object type ScheduleQuery
✔  Field getSchedFlightList was added to object type ScheduleQuery
✔  Field getScheduleGroupList was added to object type ScheduleQuery
✔  Type UrlScalar was added
✔  Type WebhookMessageInput was added
Detected 8 breaking changes
