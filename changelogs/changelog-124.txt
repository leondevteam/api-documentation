Detected the following changes (128) between schemas:

✖  Field fileHistoryList (deprecated) was removed from object type PersonalAirportEndorsement
✖  Field lastEmailSent was removed from object type PersonalAirportEndorsement
✖  Field fileHistoryList (deprecated) was removed from object type PersonalEndorsement
✖  Field lastEmailSent was removed from object type PersonalEndorsement
✖  Field PersonalEndorsement.notes changed type from String! to String
✖  Field PersonalEndorsement.number changed type from String! to String
✖  Field fileHistoryList was removed from interface PersonalEndorsementInterface
✖  Field lastEmailSent was removed from interface PersonalEndorsementInterface
✖  Field PersonalEndorsementInterface.notes changed type from String! to String
✖  Field PersonalEndorsementInterface.number changed type from String! to String
✖  Input field asmFlightNoStartsWith was added to input object type SsimSettingInput
⚠  Input field dutyManagerList was added to input object type CrewMemberCreateInput
⚠  Input field dutyManagerList was added to input object type CrewMemberUpdateInput
⚠  Enum value flight_checklist_order was added to enum EmailDataSource
⚠  Input field regCause was added to input object type FlightWatchInput
⚠  Input field plannedFuel was added to input object type JourneyLogInput
⚠  Enum value PPL was added to enum LoginLicenseType
⚠  Argument icaoTypes: [IcaoType!] added to field MyOwnerBoard.ownerFlights
⚠  Argument tripTypes: [ScheduleTripType!] added to field MyOwnerBoard.ownerFlights
⚠  Enum value airport_opening_adep was added to enum OpsMessageCategory
⚠  Enum value airport_opening_ades was added to enum OpsMessageCategory
⚠  Enum value ppr_adep was added to enum OpsMessageCategory
⚠  Enum value ppr_ades was added to enum OpsMessageCategory
⚠  Enum value schengen_adep was added to enum OpsMessageCategory
⚠  Enum value schengen_ades was added to enum OpsMessageCategory
⚠  Enum value airport_opening_adep was added to enum OpsSendMessageCategory
⚠  Enum value airport_opening_ades was added to enum OpsSendMessageCategory
⚠  Enum value ppr_adep was added to enum OpsSendMessageCategory
⚠  Enum value ppr_ades was added to enum OpsSendMessageCategory
⚠  Enum value schengen_adep was added to enum OpsSendMessageCategory
⚠  Enum value schengen_ades was added to enum OpsSendMessageCategory
⚠  Argument commercial: QuoteCommercialEnumType added to field QuoteChangeStatusMutation.setAsOption
⚠  Argument tripType: ScheduleTripType added to field QuoteChangeStatusMutation.setAsOption
⚠  Input field regCause was added to input object type SubcharterFlightWatchInput
✔  Field fixedRoutes was added to object type AcftPriceList
✔  Type AircraftFixedRoute was added
✔  Type AircraftFixedRouteAirport was added
✔  Type AircraftFixedRouteNid was added
✔  Type AircraftPriceListQuery was added
✔  Field CrewMember.dutyManager is deprecated
✔  Field CrewMember.dutyManager has deprecation reason Will be removed, use dutyManagerList node instead.
✔  Field dutyManagerList was added to object type CrewMember
✔  Field CrewMember.dutyManagerNid is deprecated
✔  Field CrewMember.dutyManagerNid has deprecation reason Will be removed, use dutyManagerList node instead.
✔  Type CrewMemberEndorsementFilterInput was added
✔  Field endorsementList was added to object type CrewMemberEndorsementSection
✔  Field legNumber was added to object type EmptyLeg
✔  Field legNumber was added to object type Flight
✔  Field regCause was added to object type FlightWatch
✔  Field regCauseSource was added to object type FlightWatch
✔  Field regCause was added to object type FlightWatchHistoryEntry
✔  Field regCauseSource was added to object type FlightWatchHistoryEntry
✔  Field getAvailableRegCauseList was added to object type FlightWatchQuery
✔  Field max_block_time_double was added to object type FtlSettings
✔  Field minimum_rest_at_homebase_after_fdp_extension was added to object type FtlSettings
✔  Field minimum_rest_at_homebase_for_time_difference was added to object type FtlSettings
✔  Field minimum_rest_at_homebase_increase_by_time_difference was added to object type FtlSettings
✔  Field minimum_rest_outside_homebase_after_fdp_extension was added to object type FtlSettings
✔  Field minimum_rest_outside_homebase_for_time_difference was added to object type FtlSettings
✔  Field minimum_rest_outside_homebase_increase_by_time_difference was added to object type FtlSettings
✔  Field require_local_night_at_homebase_for_time_difference was added to object type FtlSettings
✔  Field require_local_night_outside_homebase_for_time_difference was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_allow_after_split_duty was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_allow_before_extended_fdp was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_allow_before_split_duty was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_limit_by_preceding_fdp was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_max was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_max_longest_sector was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_max_sectors_after was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_max_sectors_after_preceding_split_duty was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_max_sectors_before was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_max_sectors_in_preceding_split_duty was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_max_time_zone_offset_diff was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_min_break_in_preceding_split_duty was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_min_next_rest was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_min_reduced_rest was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_min_sector_duration_before was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_next_fdp_decrease was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_next_rest_increase_weight was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_reduced_duration was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_require_to_precede_non_commercial_fdp was added to object type FtlSettings
✔  Field rest_reduction_home_ambulance_require_wocl was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_allow_after_split_duty was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_allow_before_extended_fdp was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_allow_before_split_duty was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_limit_by_preceding_fdp was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_max was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_max_longest_sector was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_max_sectors_after was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_max_sectors_after_preceding_split_duty was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_max_sectors_before was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_max_sectors_in_preceding_split_duty was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_max_time_zone_offset_diff was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_min_break_in_preceding_split_duty was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_min_next_rest was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_min_reduced_rest was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_min_sector_duration_before was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_next_fdp_decrease was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_next_rest_increase_weight was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_reduced_duration was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_require_to_precede_non_commercial_fdp was added to object type FtlSettings
✔  Field rest_reduction_outside_ambulance_require_wocl was added to object type FtlSettings
✔  Field weekly_rest_requirements_1 was added to object type FtlSettings
✔  Field weekly_rest_requirements_1_condition was added to object type FtlSettings
✔  Field weekly_rest_requirements_1_required_in_home was added to object type FtlSettings
✔  Field weekly_rest_requirements_1_required_length was added to object type FtlSettings
✔  Field weekly_rest_requirements_1_required_local_nights was added to object type FtlSettings
✔  Field weekly_rest_requirements_2 was added to object type FtlSettings
✔  Field weekly_rest_requirements_2_condition was added to object type FtlSettings
✔  Field weekly_rest_requirements_2_required_in_home was added to object type FtlSettings
✔  Field weekly_rest_requirements_2_required_length was added to object type FtlSettings
✔  Field weekly_rest_requirements_2_required_local_nights was added to object type FtlSettings
✔  Field salesforce was added to object type IntegrationMutation
✔  Field plannedFuel was added to object type JourneyLogHistoryEntry
✔  Field dateOfIssueIso was added to object type PersonalEndorsement
✔  Field expiresIso was added to object type PersonalEndorsement
✔  Field status was added to object type PersonalEndorsement
✔  Type PersonalEndorsementStatusEnum was added
✔  Field aircraft was added to object type PriceListQuery
✔  Type RegCauseEnum was added
✔  Type RegCauseType was added
✔  Field getCountryListByFplEet was added to object type RouteFinderQuery
✔  Type SalesforceContactMutationType was added
✔  Type SalesforceMutationType was added
✔  Field asmFlightNoStartsWith was added to object type SsimSetting
✔  Field crewList.dutyManagerList changed type from [Login!] to [Login!]!
Detected 11 breaking changes
