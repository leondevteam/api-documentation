Detected the following changes (57) between schemas:

✖  Field isDefaultCommercial (deprecated) was removed from object type Aircraft
✖  Field ownerList was removed from object type Aircraft
✖  Input field additionalHomeBaseList was added to input object type AircraftCreate
✖  Input field ownerList was removed from input object type AircraftCreate
✖  Input field ownerList was removed from input object type AircraftUpdate
✖  Input field isCommercial was removed from input object type QuoteRealizationInput
⚠  Input field additionalHomeBaseList was added to input object type AircraftUpdate
⚠  Input field dateOf4thDose was added to input object type ContactVaccinationInput
⚠  Input field dateOf5thDose was added to input object type ContactVaccinationInput
⚠  Argument aircraftNid: AircraftNid added to field CrewPanelDutyMutationSection.createQuick
⚠  Argument aircraftNid: AircraftNid added to field CrewPanelMutation.dutyCreateQuick
⚠  Enum value LANDING_PERMITS_EDIT was added to enum PermissionResource
⚠  Enum value OVERFLIGHT_PERMITS_EDIT was added to enum PermissionResource
⚠  Input field labelList was added to input object type PhonebookCompanyCreateInput
⚠  Input field labelList was added to input object type PhonebookCompanyUpdateInput
⚠  Input field labelList was added to input object type PhonebookPersonCreateInput
⚠  Input field labelList was added to input object type PhonebookPersonUpdateInput
⚠  Input field labelList was added to input object type PhonebookRepresentativeCreateInput
⚠  Input field labelList was added to input object type PhonebookRepresentativeUpdateInput
✔  Field additionalHomeBaseList was added to object type Aircraft
✔  Field tripType was added to object type Aircraft
✔  Field availableLabels was added to object type ContactQuery
✔  Field dateOf4thDose was added to object type ContactVaccination
✔  Field dateOf5thDose was added to object type ContactVaccination
✔  Field CrewPanel.crewDutiesFilter description changed from Requires access to resource GRAPHQL_CREW_PANEL.
Requires user to be authenticated using one of the following identity types: user access token, user session to Requires access to resource GRAPHQL_CREW_PANEL
✔  Field CrewPanel.filter description changed from Requires access to resource GRAPHQL_CREW_PANEL.
Requires user to be authenticated using one of the following identity types: user access token, user session to Requires access to resource GRAPHQL_CREW_PANEL
✔  Field CrewPanelMutation.setCrewDutiesFilter description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field CrewPanelMutation.setFilter description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field CrewPanelMutation.startSafteFastCalculation description changed from Requires user to be authenticated using one of the following identity types: user session.
time interval has to be longer that 1 week 4 days and shorter than 2 months to time interval has to be longer that 1 week 4 days and shorter than 2 months
✔  Field groupName was added to object type Endorsement
✔  Field sendLandingPermitMessage was added to object type FlightPermitMutation
✔  Field sendOverflightPermitMessage was added to object type FlightPermitMutation
✔  Field setLandingPermitMessageReadStatus was added to object type FlightPermitMutation
✔  Field setOverflightPermitMessageReadStatus was added to object type FlightPermitMutation
✔  Type FlightPermitQuery was added
✔  Field require_extended_rest_on_home_base_change was added to object type FtlSettings
✔  Field HistorySection.myActivityHistory description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Type LandingPermitNid was added
✔  Field LoggedUser.flight description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field LoggedUser.flights description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field LoggedUser.ownerBoard description changed from Requires access to resource GRAPHQL_OWNER_BOARD_SEE.
Requires user to be authenticated using one of the following identity types: user access token, user session to Requires access to resource GRAPHQL_OWNER_BOARD_SEE
✔  Field LoggedUser.positionings description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field MyDutyType.myTrRevision description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field ldgUTC was added to object type MyJourneyLog
✔  Field toUTC was added to object type MyJourneyLog
✔  Field MyOwnerBoard.ownerConfig description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field MyOwnerBoard.ownerFlights description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Type OverflightPermitNid was added
✔  Field PermissionLevel.level description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field isDeleted was added to object type PhonebookCompany
✔  Type PhonebookLabelInput was added
✔  Field isDeleted was added to object type PhonebookPerson
✔  Field flightPermits was added to object type Query
✔  Field Query.permission description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field Query.privilegeLevel description changed from Requires user to be authenticated using one of the following identity types: user access token, user session to ''
✔  Field defaultDueTimeForOptionalTrips was added to object type settingsSales
✔  Field isUpdateQuoteOnScheduleChange was added to object type settingsSales
Detected 6 breaking changes
