Detected the following changes (102) between schemas:

⚠  Input field name was added to input object type AirportSubPriceInput
⚠  Input field aircraft was added to input object type CrewPanelPositioningInput
⚠  Enum value AMOS_INTEGRATION_ERROR was added to enum ErrorCategory
⚠  Input field aircraft was added to input object type PositioningCreate
⚠  Input field aircraft was added to input object type PositioningUpdate
✔  Field AirportDirectory.hoursRecurrence description changed from ' to Requires access to resource GRAPHQL_AIRPORT_DIRECTORY_SEE'
✔  Type AmosIntegrationConfiguration was added
✔  Type AmosIntegrationMutation was added
✔  Type AmosIntegrationSettings was added
✔  Type AmosProtocol was added
✔  Field defaultComment was added to object type ChecklistDefType
✔  Field defaultLabel was added to object type ChecklistDefType
✔  Type CrewAppAOC was added
✔  Type CrewAppAircraft was added
✔  Type CrewAppAirport was added
✔  Type CrewAppAirportNotes was added
✔  Type CrewAppAptdirHandler was added
✔  Type CrewAppAptdirRestriction was added
✔  Type CrewAppAptdirSupplier was added
✔  Type CrewAppCargoHandlingAdepChecklistItem was added
✔  Type CrewAppCargoHandlingAdesChecklistItem was added
✔  Type CrewAppCateringChecklistItem was added
✔  Type CrewAppChecklist was added
✔  Type CrewAppChecklistDefType was added
✔  Type CrewAppChecklistItem was added
✔  Type CrewAppChecklistQuery was added
✔  Type CrewAppChecklistStatusType was added
✔  Type CrewAppContact was added
✔  Type CrewAppCountry was added
✔  Type CrewAppCrewMemberOnLeg was added
✔  Type CrewAppCrewProperty was added
✔  Type CrewAppDocumentElement was added
✔  Type CrewAppEapisChecklistItem was added
✔  Type CrewAppFileSimple was added
✔  Type CrewAppFlight was added
✔  Type CrewAppFlightAndTripChecklistItemType was added
✔  Type CrewAppFlightFuelPrices was added
✔  Type CrewAppFlightFuelSupplier was added
✔  Type CrewAppFlightNotesType was added
✔  Type CrewAppFlightQuery was added
✔  Type CrewAppFlightWatch was added
✔  Type CrewAppFuelChecklistItem was added
✔  Type CrewAppHandlingAdepChecklistItem was added
✔  Type CrewAppHandlingAdesChecklistItem was added
✔  Type CrewAppHil was added
✔  Type CrewAppHotel was added
✔  Type CrewAppHotelChecklistItem was added
✔  Type CrewAppLandingPermissionChecklistItem was added
✔  Type CrewAppLegPermit was added
✔  Type CrewAppLimousineAdepChecklistItem was added
✔  Type CrewAppLimousineAdesChecklistItem was added
✔  Type CrewAppLogin was added
✔  Type CrewAppLuggageChecklistItem was added
✔  Type CrewAppOverflightPermissionChecklistItem was added
✔  Type CrewAppPaxTransportAdepChecklistItem was added
✔  Type CrewAppPaxTransportAdesChecklistItem was added
✔  Type CrewAppPosition was added
✔  Type CrewAppPositioning was added
✔  Type CrewAppPositioningQuery was added
✔  Field flight was added to object type CrewAppQuery
✔  Field positioning was added to object type CrewAppQuery
✔  Type CrewAppSlotAdepChecklistItem was added
✔  Type CrewAppSlotAdesChecklistItem was added
✔  Type CrewAppTagDefinition was added
✔  Type CrewAppTrip was added
✔  Type CrewAppTripNotes was added
✔  Field standby_fdp_reduction_disable_if_night_time_at_least was added to object type FtlSettings
✔  Type IntegrationSettings was added
✔  Field Mutation.airportDirectory description changed from ' to Requires access to resource GRAPHQL_AIRPORT_DIRECTORY_EDIT'
✔  Type NonNullCrewAppCargoHandlingAdepChecklistItemValue was added
✔  Type NonNullCrewAppCargoHandlingAdesChecklistItemValue was added
✔  Type NonNullCrewAppCateringChecklistItemValue was added
✔  Type NonNullCrewAppChecklistItemValue was added
✔  Type NonNullCrewAppEapisChecklistItemValue was added
✔  Type NonNullCrewAppFuelChecklistItemValue was added
✔  Type NonNullCrewAppHandlingAdepChecklistItemValue was added
✔  Type NonNullCrewAppHandlingAdesChecklistItemValue was added
✔  Type NonNullCrewAppHotelChecklistItemValue was added
✔  Type NonNullCrewAppLandingPermissionChecklistItemValue was added
✔  Type NonNullCrewAppLimousineAdepChecklistItemValue was added
✔  Type NonNullCrewAppLimousineAdesChecklistItemValue was added
✔  Type NonNullCrewAppLuggageChecklistItemValue was added
✔  Type NonNullCrewAppOverflightPermissionChecklistItemValue was added
✔  Type NonNullCrewAppPaxTransportAdepChecklistItemValue was added
✔  Type NonNullCrewAppPaxTransportAdesChecklistItemValue was added
✔  Type NonNullCrewAppSlotAdepChecklistItemValue was added
✔  Type NonNullCrewAppSlotAdesChecklistItemValue was added
✔  Field setChecklistItemDefaultComment was added to object type OperatorChecklistSettingsMutation
✔  Field setChecklistItemLabel was added to object type OperatorChecklistSettingsMutation
✔  Type OperatorIntegrationSettingsMutation was added
✔  Field integrationSettings was added to object type OperatorMutation
✔  Field integrationSettings was added to object type OperatorSetting
✔  Field aircraft was added to object type Positioning
✔  Field Query.integration changed type from IntegrationQueries to IntegrationQueries!
✔  Type QuoteMutation was added
✔  Field singlePriceName was added to object type QuoteRealizationAptSubPrice
✔  Type QuoteSetUsedInput was added
✔  Type QuoteTripDeleteInput was added
✔  Type QuoteTripMutation was added
✔  Type QuoteTripUpdateInput was added
✔  Field quote was added to object type SalesMutation
✔  Field aircraftSalesList was added to object type SalesQuery
No breaking changes detected
