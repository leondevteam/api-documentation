Detected the following changes (32) between schemas:

✖  Input field receivedDateTime was added to input object type InteractiveApiReportStatusInput
⚠  Enum value _3DAZI was added to enum ApproachType
⚠  Enum value _3DCDI was added to enum ApproachType
⚠  Enum value dot_3d_azi_curr was added to enum CurrencyIdEnum
⚠  Enum value dot_3d_azi_curr_all was added to enum CurrencyIdEnum
⚠  Enum value dot_3d_cdi_curr was added to enum CurrencyIdEnum
⚠  Enum value dot_3d_cdi_curr_all was added to enum CurrencyIdEnum
⚠  Argument opsNotes: String added to field ScheduleMutation.modifySchedGroupFlights
✔  Field airframeLimitNote was added to object type Aircraft
✔  Field airframeStartDate was added to object type Aircraft
✔  Field airframeStartTac was added to object type Aircraft
✔  Field airframeStartTah was added to object type Aircraft
✔  Field exportToCAMP was added to object type Aircraft
✔  Field oilReportPeriodHours was added to object type AircraftEngine
✔  Field startTac was added to object type AircraftEngine
✔  Field startTah was added to object type AircraftEngine
✔  Field adep was added to object type DefaultPassengerData
✔  Field ades was added to object type DefaultPassengerData
✔  Field DefaultPassengerData.airport is deprecated
✔  Field DefaultPassengerData.airport has deprecation reason Replaced by ades field
✔  Field allow_five_night_duties_alternative was added to object type FtlSettings
✔  Field require_day_off_after_multiple_night_duties was added to object type FtlSettings
✔  Field myDuty was added to object type LoggedUser
✔  Type MyDutyOutput was added
✔  Type MyDutyViolation was added
✔  Type MyDutyViolationEnum was added
✔  Type MyDutyViolationList was added
✔  Type NonNullMyDutyTypeValue was added
✔  Field QuoteRequest.providerType has deprecation reason Use `providerTypeEnum` field instead
✔  Field QuoteRequest.providerType is deprecated
✔  Field providerTypeEnum was added to object type QuoteRequest
✔  Type QuoteRequestProviderEnum was added
Detected 1 breaking change
