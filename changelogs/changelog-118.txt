Detected the following changes (144) between schemas:

✖  Field ChecklistMutation.addFilesToOpsItem changed type from File! to [File!]!
✖  Field ChecklistMutation.addFilesToSalesItem changed type from File! to [File!]!
✖  Field JourneyLog.bag changed type from Int! to Int
✖  Field JourneyLog.bagsQuantity changed type from Int! to Int
✖  Field JourneyLog.cargo changed type from Int! to Int
✖  Field JourneyLog.cargoUnit changed type from Int! to Int
✖  Field JourneyLog.paxChildCount changed type from Int! to Int
✖  Field JourneyLog.paxCount changed type from Int! to Int
✖  Field JourneyLog.paxFemaleCount changed type from Int! to Int
✖  Field JourneyLog.paxHeadsCount changed type from Int! to Int
✖  Field JourneyLog.paxInBusinessClassCount changed type from Int! to Int
✖  Field JourneyLog.paxInEconomyClassCount changed type from Int! to Int
✖  Field JourneyLog.paxInTransitCount changed type from Int! to Int
✖  Field JourneyLog.paxInfantCount changed type from Int! to Int
✖  Field JourneyLog.paxMaleCount changed type from Int! to Int
✖  Field JourneyLog.paxPetCount changed type from Int! to Int
✖  Field JourneyLog.slingLoad changed type from Int! to Int
✖  Field JourneyLog.underload changed type from Int! to Int
✖  Field MailboxConfig.allowTelexRecipients changed type from MailboxTelexEnum! to MailboxTelexEnum
✖  Field MailboxConfig.smtpLogin changed type from String! to String
✖  Field MailboxConfig.smtpPassword changed type from String! to String
✖  Field MailboxConfig.smtpPort changed type from Int! to Int
✖  Field MailboxConfig.smtpProtocol changed type from MailboxProtocolEnum! to MailboxProtocolEnum
✖  Field MailboxConfig.smtpUrl changed type from String! to String
✖  Input field authorization was added to input object type MailboxConfigInput
✖  Input field authorization was added to input object type MailboxConfigValidateInput
✖  Argument dontSendEmptyReports: Boolean! added to field ReportWizardScheduleMutation.create
✖  Argument dontSendEmptyReports: Boolean! added to field ReportWizardScheduleMutation.update
⚠  Argument endTime: DateTime added to field AircraftAvailabilitySection.emptyLegList
⚠  Input field dutyEndTime was added to input object type CrewPanelSimulatorInput
⚠  Input field reportingTime was added to input object type CrewPanelSimulatorInput
⚠  Input field takeOffWeight was added to input object type JourneyLogInput
⚠  Input field userLabels was added to input object type PhonebookFilter
⚠  Input field isRepresentative was added to input object type PhonebookPersonCreateInput
⚠  Input field nationality was added to input object type PhonebookPersonCreateInput
⚠  Input field weight was added to input object type PhonebookPersonCreateInput
⚠  Input field isRepresentative was added to input object type PhonebookPersonUpdateInput
⚠  Input field nationality was added to input object type PhonebookPersonUpdateInput
⚠  Input field weight was added to input object type PhonebookPersonUpdateInput
⚠  Input field isRepresentative was added to input object type PhonebookRepresentativeCreateInput
⚠  Input field nationality was added to input object type PhonebookRepresentativeCreateInput
⚠  Input field weight was added to input object type PhonebookRepresentativeCreateInput
⚠  Input field nationality was added to input object type PhonebookRepresentativeUpdateInput
⚠  Input field weight was added to input object type PhonebookRepresentativeUpdateInput
⚠  Input field documents was added to input object type QuoteRealizationSubcharterInput
⚠  Enum value next3days was added to enum ReportWizardDateRange
⚠  Enum value fourweekly was added to enum RotationType
⚠  Input field dutyEndTime was added to input object type SimulatorInput
⚠  Input field reportingTime was added to input object type SimulatorInput
✔  Field address was added to object type Contact
✔  Field code was added to object type Contact
✔  Field emailBilling was added to object type Contact
✔  Field emailList was added to object type Contact
✔  Field emailWork was added to object type Contact
✔  Field emailWork2 was added to object type Contact
✔  Field exclusionZones was added to object type Contact
✔  Field genderEnum was added to object type Contact
✔  Field isAssignedAccount was added to object type Contact
✔  Field isClient was added to object type Contact
✔  Field isDeleted was added to object type Contact
✔  Field isRepresentative was added to object type Contact
✔  Field isValidToEdit was added to object type Contact
✔  Field nationality was added to object type Contact
✔  Field operatorBase was added to object type Contact
✔  Field otherSettings was added to object type Contact
✔  Field phoneList was added to object type Contact
✔  Field remarks was added to object type Contact
✔  Field salesforceId was added to object type Contact
✔  Field salutation was added to object type Contact
✔  Field weight was added to object type Contact
✔  Type ContactWeight was added
✔  Field dutyEndTime was added to object type CrewPanelSimulator
✔  Field reportingTime was added to object type CrewPanelSimulator
✔  Type DutyRequestStatus was added
✔  Field takeOffWeightWithUnit was added to object type JourneyLogHistoryEntry
✔  Field dutyRequestList was added to object type LoggedUser
✔  Type MailboxAuthorizationEnum was added
✔  Field authorization was added to object type MailboxConfig
✔  Input field MailboxConfigInput.allowTelexRecipients changed type from MailboxTelexEnum! to MailboxTelexEnum
✔  Input field MailboxConfigInput.smtpLogin changed type from String! to String
✔  Input field MailboxConfigInput.smtpPassword changed type from String! to String
✔  Input field MailboxConfigInput.smtpPort changed type from Int! to Int
✔  Input field MailboxConfigInput.smtpProtocol changed type from MailboxProtocolEnum! to MailboxProtocolEnum
✔  Input field MailboxConfigInput.smtpUrl changed type from String! to String
✔  Type MailboxConfigOAuthResult was added
✔  Field googleAuthorization was added to object type MailboxConfigQuerySection
✔  Type MailboxConfigSubscriptionSection was added
✔  Input field MailboxConfigValidateInput.imapLogin changed type from String! to String
✔  Input field MailboxConfigValidateInput.imapPassword changed type from String! to String
✔  Input field MailboxConfigValidateInput.imapPort changed type from Int! to Int
✔  Input field MailboxConfigValidateInput.imapProtocol changed type from MailboxProtocolEnum! to MailboxProtocolEnum
✔  Input field MailboxConfigValidateInput.imapUrl changed type from String! to String
✔  Field isHtml was added to object type MailboxMessage
✔  Type MyDutyRequest was added
✔  Type NonNullContactValue was added
✔  Type NonNullContactValueOrErrorList was added
✔  Type OAuthAuthorizationUrl was added
✔  Field nationality was added to object type PhonebookCompanyRepresentative
✔  Field weight was added to object type PhonebookCompanyRepresentative
✔  Field companyCreate was added to object type PhonebookMutation
✔  Field companyUpdate was added to object type PhonebookMutation
✔  Field PhonebookMutation.createCompany is deprecated
✔  Field PhonebookMutation.createCompany has deprecation reason Use companyCreate instead
✔  Field PhonebookMutation.createPerson is deprecated
✔  Field PhonebookMutation.createPerson has deprecation reason Use personCreate instead
✔  Field PhonebookMutation.createRepresentative has deprecation reason Use representativeCreate instead
✔  Field PhonebookMutation.createRepresentative is deprecated
✔  Field personCreate was added to object type PhonebookMutation
✔  Field personUpdate was added to object type PhonebookMutation
✔  Field representativeCreate was added to object type PhonebookMutation
✔  Field representativeUpdate was added to object type PhonebookMutation
✔  Field PhonebookMutation.updateCompany has deprecation reason Use companyUpdate instead
✔  Field PhonebookMutation.updateCompany is deprecated
✔  Field PhonebookMutation.updatePerson is deprecated
✔  Field PhonebookMutation.updatePerson has deprecation reason Use personUpdate instead
✔  Field PhonebookMutation.updateRepresentative has deprecation reason Use representativeUpdate instead
✔  Field PhonebookMutation.updateRepresentative is deprecated
✔  Field isRepresentative was added to object type PhonebookPerson
✔  Field nationalIdList was added to object type PhonebookPerson
✔  Field nationality was added to object type PhonebookPerson
✔  Field passportList was added to object type PhonebookPerson
✔  Field weight was added to object type PhonebookPerson
✔  Field PhonebookQuerySection.company has deprecation reason Use ContactQuery instead
✔  Field PhonebookQuerySection.company is deprecated
✔  Field PhonebookQuerySection.person is deprecated
✔  Field PhonebookQuerySection.person has deprecation reason Use ContactQuery instead
✔  Field PhonebookQuerySection.representative has deprecation reason Use ContactQuery instead
✔  Field PhonebookQuerySection.representative is deprecated
✔  Field documents was added to object type QuoteRealizationSubcharter
✔  Field rejectDetails was added to object type QuoteRequest
✔  Field isHtml was added to object type QuoteRequestMessageContent
✔  Type QuoteRequestRejectDetails was added
✔  Field dontSendEmptyReports was added to object type ReportWizardSchedule
✔  Field downloadAvinodeRequest was added to object type SalesMutation
✔  Field dutyEndTime was added to object type Simulator
✔  Field dutyEndTimeInTimezone was added to object type Simulator
✔  Field reportingTime was added to object type Simulator
✔  Field reportingTimeInTimezone was added to object type Simulator
✔  Type SubcharterDocument was added
✔  Type SubcharterDocumentInput was added
✔  Type SubcharterDocumentNid was added
✔  Field isHtml was added to object type SubcharterMailboxMessage
✔  Field isHtml was added to object type SubcharterMessageContent
✔  Field mailboxConfig was added to object type Subscription
Detected 28 breaking changes
