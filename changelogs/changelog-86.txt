Detected the following changes (154) between schemas:

✖  Field AircraftCrewTemporary.nid changed type from ID! to AircraftCrewTemporaryNidScalar!
✖  Field CrewMemberEndorsementMutation.deleteUsingExternalId changed type from NonNullBooleanValueOrErrorList! to CrewMemberEndorsementDeleteUsingExternalIdOutput!
✖  Field CrewMemberEndorsementMutation.putUsingExternalId changed type from NonNullPersonalEndorsementValueOrErrorList! to CrewMemberEndorsementPutUsingExternalIdOutput!
✖  Field CrewMemberMutationRating.setUsingExternalId changed type from NonNullBooleanValueOrErrorList! to CrewMemberRatingSetUsingExternalIdOutput!
✖  Field CrewMemberMutationSection.create changed type from NonNullCrewMemberValueOrErrorList! to CrewMemberCreateOutput!
✖  Field CrewMemberMutationSection.deleteUsingExternalId changed type from NonNullBooleanValueOrErrorList! to CrewMemberDeleteUsingExternalIdOutput!
✖  Field CrewMemberMutationSection.updateUsingExternalId changed type from NonNullCrewMemberValueOrErrorList! to CrewMemberUpdateUsingExternalIdOutput!
✖  Field deleteInPeriod was removed from object type CrewMemberMutationTemporaryAssignment
✖  Field CrewMemberPassportMutationSection.deleteUsingExternalId changed type from NonNullBooleanValueOrErrorList! to CrewMemberPassportDeleteUsingExternalIdOutput!
✖  Field CrewMemberPassportMutationSection.putUsingExternalId changed type from NonNullPassportOutputValueOrErrorList! to CrewMemberPassportPutUsingExternalIdOutput!
✖  Field CrewMemberVisaMutationSection.deleteUsingExternalId changed type from NonNullBooleanValueOrErrorList! to CrewMemberPassportVisaDeleteUsingExternalIdOutput!
✖  Field CrewMemberVisaMutationSection.putUsingExternalId changed type from NonNullPassportVisaValueOrErrorList! to CrewMemberPassportVisaPutUsingExternalIdOutput!
✖  Input field useMessageCache was added to input object type MailboxConfigInput
✖  Enum value slots was removed from enum MultiLegServiceTypeEnum
✖  Type NonNullPassportOutputValueOrErrorList was removed
✖  Type NonNullPassportVisaValueOrErrorList was removed
✖  Type NonNullPersonalEndorsementValueOrErrorList was removed
✖  Field PassportVisa.country changed type from Country! to PassportVisaCountry!
✖  Type for argument quoteRequestStatuses on field SalesQuery.findQuoteRequestsWithRealizations changed from [QuoteRequestStatusEnum] to [QuoteRequestStatusEnum!]
✖  Type for argument quoteRequestStatuses on field SalesQuery.findSubcharterQuoteRealizations changed from [QuoteRequestStatusEnum] to [QuoteRequestStatusEnum!]
✖  Field crewList.crewMemberUsingExternalId changed type from NonNullCrewMemberValueOrErrorList! to CrewMemberUsingExternalIdOutput!
⚠  Enum value CREW_MEMBER_CODE_DUPLICATE was added to enum ErrorCategory
⚠  Enum value CREW_MEMBER_EMAIL_DUPLICATE was added to enum ErrorCategory
⚠  Enum value CREW_MEMBER_EXTERNAL_ID_DUPLICATE was added to enum ErrorCategory
⚠  Enum value CREW_MEMBER_LOGIN_DUPLICATE was added to enum ErrorCategory
⚠  Enum value CREW_MEMBER_NOT_FOUND was added to enum ErrorCategory
⚠  Enum value ENDORSEMENT_DEFINITION_NOT_FOUND was added to enum ErrorCategory
⚠  Enum value QUOTE_APT_DISCONTINUITY was added to enum FeasibilityCheckValidation
⚠  Argument includeMyAircraftDuties: Boolean added to field LoggedUser.duties
⚠  Enum value catering was added to enum MultiLegServiceTypeEnum
⚠  Enum value handling_adep was added to enum MultiLegServiceTypeEnum
⚠  Enum value handling_ades was added to enum MultiLegServiceTypeEnum
⚠  Enum value pax_transport_adep was added to enum MultiLegServiceTypeEnum
⚠  Enum value pax_transport_ades was added to enum MultiLegServiceTypeEnum
⚠  Enum value slot_adep was added to enum MultiLegServiceTypeEnum
⚠  Enum value slot_ades was added to enum MultiLegServiceTypeEnum
⚠  Input field cateringSupplierNid was added to input object type MultiLegSuppliersInput
⚠  Input field handlingAdepHandlerNid was added to input object type MultiLegSuppliersInput
⚠  Input field handlingAdesHandlerNid was added to input object type MultiLegSuppliersInput
⚠  Input field paxTransportAdepSupplierNid was added to input object type MultiLegSuppliersInput
⚠  Input field paxTransportAdesSupplierNid was added to input object type MultiLegSuppliersInput
⚠  Input field countryOfIssue was added to input object type PassportVisaPutInput
⚠  Input field isSchengen was added to input object type PassportVisaPutInput
⚠  Input field emailData was added to input object type QuoteRequestAgreementEnvelopeInput
⚠  Argument clientsList: [ClientNid!] added to field SalesQuery.findQuoteRequestsWithRealizations
⚠  Argument clientsList: [ClientNid!] added to field SalesQuery.findSubcharterQuoteRealizations
⚠  Argument clientsList: [ClientNid!] added to field TripQuery.bookingsTripList
✔  Type AircraftCrewTemporaryNidScalar was added
✔  Field getCateringSuppliers was added to object type AirportDirectory
✔  Field getPaxTransportSuppliers was added to object type AirportDirectory
✔  Type AptdirSupplier was added
✔  Type AptdirSupplierNid was added
✔  Type CrewMemberCreateError was added
✔  Type CrewMemberCreateErrorList was added
✔  Type CrewMemberCreateOutput was added
✔  Type CrewMemberDeleteUsingExternalIdError was added
✔  Type CrewMemberDeleteUsingExternalIdErrorList was added
✔  Type CrewMemberDeleteUsingExternalIdOutput was added
✔  Type CrewMemberEndorsementDeleteUsingExternalIdError was added
✔  Type CrewMemberEndorsementDeleteUsingExternalIdErrorList was added
✔  Type CrewMemberEndorsementDeleteUsingExternalIdOutput was added
✔  Type CrewMemberEndorsementPutUsingExternalIdError was added
✔  Type CrewMemberEndorsementPutUsingExternalIdErrorList was added
✔  Type CrewMemberEndorsementPutUsingExternalIdOutput was added
✔  Field putUsingExternalId was added to object type CrewMemberMutationRating
✔  Field delete was added to object type CrewMemberMutationTemporaryAssignment
✔  Field flightTrainingPilotNote was added to object type CrewMemberOnLeg
✔  Type CrewMemberPassportDeleteUsingExternalIdError was added
✔  Type CrewMemberPassportDeleteUsingExternalIdErrorList was added
✔  Type CrewMemberPassportDeleteUsingExternalIdOutput was added
✔  Type CrewMemberPassportPutUsingExternalIdError was added
✔  Type CrewMemberPassportPutUsingExternalIdErrorList was added
✔  Type CrewMemberPassportPutUsingExternalIdOutput was added
✔  Type CrewMemberPassportVisaDeleteUsingExternalIdError was added
✔  Type CrewMemberPassportVisaDeleteUsingExternalIdErrorList was added
✔  Type CrewMemberPassportVisaDeleteUsingExternalIdOutput was added
✔  Type CrewMemberPassportVisaPutUsingExternalIdError was added
✔  Type CrewMemberPassportVisaPutUsingExternalIdErrorList was added
✔  Type CrewMemberPassportVisaPutUsingExternalIdOutput was added
✔  Type CrewMemberRatingSetUsingExternalIdError was added
✔  Type CrewMemberRatingSetUsingExternalIdErrorList was added
✔  Type CrewMemberRatingSetUsingExternalIdOutput was added
✔  Type CrewMemberUpdateUsingExternalIdError was added
✔  Type CrewMemberUpdateUsingExternalIdErrorList was added
✔  Type CrewMemberUpdateUsingExternalIdOutput was added
✔  Type CrewMemberUsingExternalIdError was added
✔  Type CrewMemberUsingExternalIdErrorList was added
✔  Type CrewMemberUsingExternalIdOutput was added
✔  Field CrewMemberVisaMutationSection.putUsingExternalId description changed from Requires access to resource GRAPHQL_CREW_MEMBER_EDIT to Requires access to resource GRAPHQL_CREW_MEMBER_EDIT.
If visa is for schengen, country parameter must be empty.
✔  Field endAirport was added to object type CrewPanelDraftDay
✔  Field CrewPanelDraftDay.endApt is deprecated
✔  Field CrewPanelDraftDay.endApt has deprecation reason Please use endAirport field instead
✔  Field startAirport was added to object type CrewPanelDraftDay
✔  Field CrewPanelDraftDay.startApt is deprecated
✔  Field CrewPanelDraftDay.startApt has deprecation reason Please use startAirport field instead
✔  Field endAirport was added to object type CrewPanelDuty
✔  Field CrewPanelDuty.endApt is deprecated
✔  Field CrewPanelDuty.endApt has deprecation reason Please use endAirport field instead
✔  Field hasNote was added to object type CrewPanelDuty
✔  Field startAirport was added to object type CrewPanelDuty
✔  Field CrewPanelDuty.startApt is deprecated
✔  Field CrewPanelDuty.startApt has deprecation reason Please use startAirport field instead
✔  Field endAirport was added to object type CrewPanelFlight
✔  Field CrewPanelFlight.endApt has deprecation reason Please use endAirport field instead
✔  Field CrewPanelFlight.endApt is deprecated
✔  Field startAirport was added to object type CrewPanelFlight
✔  Field CrewPanelFlight.startApt is deprecated
✔  Field CrewPanelFlight.startApt has deprecation reason Please use startAirport field instead
✔  Field endAirport was added to object type CrewPanelPositioning
✔  Field CrewPanelPositioning.endApt has deprecation reason Please use endAirport field instead
✔  Field CrewPanelPositioning.endApt is deprecated
✔  Field startAirport was added to object type CrewPanelPositioning
✔  Field CrewPanelPositioning.startApt has deprecation reason Please use startAirport field instead
✔  Field CrewPanelPositioning.startApt is deprecated
✔  Field CrewPanelSimulator.endApt has deprecation reason Please use endAirport field instead
✔  Field CrewPanelSimulator.endApt is deprecated
✔  Field CrewPanelSimulator.startApt has deprecation reason Please use startAirport field instead
✔  Field CrewPanelSimulator.startApt is deprecated
✔  Type DocusignEmailInput was added
✔  Field flightListUpdate was added to object type FlightMutationQuery
✔  Type FlightUpdateInput was added
✔  Field include_options_in_ftl was added to object type FtlSettings
✔  Field min_rest_before_augmented_crew was added to object type FtlSettings
✔  Field use_aircraft_home_base_as_crew_home_base was added to object type FtlSettings
✔  Field use_most_restrictive_fdp_limit was added to object type FtlSettings
✔  Field isRequest was added to object type HistoryActivityDuty
✔  Field routefinder was added to object type IntegrationQueries
✔  Field useMessageCache was added to object type MailboxConfig
✔  Field servicesList was added to object type MultiLegRequest
✔  Field cateringSupplier was added to object type MultiLegSuppliers
✔  Field handlingAdepHandler was added to object type MultiLegSuppliers
✔  Field handlingAdesHandler was added to object type MultiLegSuppliers
✔  Field paxTransportAdepSupplier was added to object type MultiLegSuppliers
✔  Field paxTransportAdesSupplier was added to object type MultiLegSuppliers
✔  Field Mutation.flightUpdate is deprecated
✔  Field Mutation.flightUpdate has deprecation reason use flights.flightListUpdate mutation
✔  Field crewCode was added to object type MyDutyType
✔  Field isMyDuty was added to object type MyDutyType
✔  Type NonNullCountryValue was added
✔  Type NonNullListOfNonNullRouteFinderFlightTimeCalculationValue was added
✔  Type NonNullListOfNonNullRouteFinderFlightTimeCalculationValueOrErrorList was added
✔  Type NonNullPassportVisaSchengenValue was added
✔  Field countryOfIssue was added to object type PassportVisa
✔  Type PassportVisaCountry was added
✔  Input field PassportVisaPutInput.country changed type from CountryCode! to CountryCode
✔  Type PassportVisaSchengen was added
✔  Field thirdPartyCommission was added to object type Pricing
✔  Field Query.flightList description changed from Requires access to resource GRAPHQL_FLIGHT.
Requires access to given flight to Returns flights data for given period using specified filter settings. Period cannot be greater that 3 months..
Requires access to resource GRAPHQL_FLIGHT.
Requires access to given flight
✔  Field checklistSalesDotColor was added to object type QuoteRealizationSubcharter
✔  Type RouteFinderFlightTimeCalculation was added
✔  Type RouteFinderFlightTimeCalculationInput was added
✔  Type RouteFinderQuery was added
✔  Field getQuoteRealizationsClientsList was added to object type SalesQuery
✔  Field getTripsClientsList was added to object type TripQuery
Detected 21 breaking changes
