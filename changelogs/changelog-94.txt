Detected the following changes (90) between schemas:

✖  Input field AircraftInsuranceExcludedCountryInput.allowedAirports changed type from [AirportCodeScalar!]! to [AirportCodeScalar!]
✖  Field dateOfBirthTimestamp (deprecated) was removed from object type Contact
✖  Field CrewMemberPassportMutationSection.putUsingExternalId changed type from CrewMemberPassportPutUsingExternalIdOutput! to NonNullPassportOutputValueOrErrorList!
✖  Argument passportNumber: String! was removed from field CrewMemberPassportMutationSection.putUsingExternalId
✖  Type CrewMemberPassportPutUsingExternalIdError was removed
✖  Type CrewMemberPassportPutUsingExternalIdErrorList was removed
✖  Type CrewMemberPassportPutUsingExternalIdOutput was removed
✖  Field CrewPanelCrewMutationSection.assign changed type from Boolean! to NonNullBooleanValueOrErrorList!
✖  Field CrewPanelMutation.assignCrew changed type from Boolean! to NonNullBooleanValueOrErrorList!
✖  Input field aocSource was added to input object type CrewPanelSimulatorInput
✖  Input field PassportPutInput.externalId changed type from String to String!
✖  Input field number was added to input object type PassportPutInput
✖  Field PhonebookCompanyRepresentative.additionalCompanies changed type from [PhonebookCompany!]! to [Contact!]!
✖  Field PhonebookCompanyRepresentative.company changed type from PhonebookCompany! to Contact
✖  Field PhonebookCompanyRepresentative.representativeNid changed type from RepresentativeNid! to ContactPersonNid!
✖  Type for argument representativeNid on field PhonebookQuerySection.representative changed from RepresentativeNid! to ContactPersonNid!
✖  Input field PhonebookRepresentativeCreateInput.company changed type from ClientCompanyNid! to ContactCompanyNid!
✖  Input field PhonebookRepresentativeUpdateInput.company changed type from ClientCompanyNid to ContactCompanyNid
✖  Input field PhonebookRepresentativeUpdateInput.representativeNid changed type from RepresentativeNid! to ContactPersonNid!
✖  Input field aocSource was added to input object type SimulatorInput
✖  Input field docType was added to input object type TravelDocumentsContactInput
⚠  Input field excludedAirports was added to input object type AircraftInsuranceExcludedCountryInput
⚠  Input field aoc was added to input object type CrewPanelSimulatorInput
⚠  Input field extendedRange was added to input object type FlightUpdateInput
⚠  Input field bagsQuantity was added to input object type JourneyLogInput
⚠  Input field paxHeads was added to input object type JourneyLogInput
⚠  Argument leadPassenger: ContactNid added to field Mutation.addPassengersToList
⚠  Argument leadPassenger: ContactNid added to field Mutation.savePassengerList
⚠  Argument leadPassenger: ContactNid added to field PassengerListMutation.savePassengerList
⚠  Enum value GRAPHQL_AIRCRAFT_INSURANCE_EDIT was added to enum PermissionResource
⚠  Enum value GRAPHQL_AIRCRAFT_INSURANCE_SEE was added to enum PermissionResource
⚠  Argument clientCompanyNid: ClientCompanyNid added to field Query.passengerByWildcard
⚠  Argument offset: Int added to field Query.passengerByWildcard
⚠  Argument quoteRequestStatuses: [QuoteRequestStatusEnum!] added to field SalesQuery.findQuoteRequestsWithRealizations
⚠  Input field aoc was added to input object type SimulatorInput
✔  Field AircraftInsuranceExcludedCountry.allowedAirports has deprecation reason Use excludedAirports.
✔  Field AircraftInsuranceExcludedCountry.allowedAirports is deprecated
✔  Field excludedAirports was added to object type AircraftInsuranceExcludedCountry
✔  Type AocNid was added
✔  Type AocSource was added
✔  Type ChangedInOpsRealizationMutation was added
✔  Field addOrUpdateOpsItems was added to object type ChecklistMutation
✔  Field addOrUpdateSalesItems was added to object type ChecklistMutation
✔  Field removeOpsItems was added to object type ChecklistMutation
✔  Field removeSalesItems was added to object type ChecklistMutation
✔  Field additionalCompanies was added to object type Contact
✔  Field company was added to object type Contact
✔  Field clientCompanyByWildcard was added to object type ContactQuery
✔  Field contactsByNids was added to object type ContactQuery
✔  Field aoc was added to object type CrewPanelSimulator
✔  Field aocSource was added to object type CrewPanelSimulator
✔  Description Flight Watch for a single flight on type FlightWatch has changed to Flight Watch for a single flight or quote realisation subcharter leg
✔  Field FlightWatch.activityType is deprecated
✔  Field FlightWatch.activityType has deprecation reason Field removed
✔  Field FlightWatch.duration is deprecated
✔  Field FlightWatch.duration has deprecation reason Use `calculateDuration` field
✔  Field FlightWatch.endTime is deprecated
✔  Field FlightWatch.endTime has deprecation reason Use onBlock field
✔  Field FlightWatch.isActive has deprecation reason Field removed
✔  Field FlightWatch.isActive is deprecated
✔  Field FlightWatch.startTime is deprecated
✔  Field FlightWatch.startTime has deprecation reason Use offBlock field
✔  Field FlightWatchMutation.modifyFlightWatch changed type from FlightWatch to FlightWatch!
✔  Field modifySubcharterFlightWatch was added to object type FlightWatchMutation
✔  Field FlightWatchQuery.flightWatchList description changed from Returns only revisions accessible for logged user considering aircraft privileges.
Requires access to resource GRAPHQL_FLIGHT_WATCH.
Requires access to given flight to Requires access to resource GRAPHQL_FLIGHT_WATCH.
Requires access to given flight
✔  Field JourneyLog.bag description changed from ' to Bags [mass]'
✔  Field bagsQuantity was added to object type JourneyLog
✔  Field paxHeadsCount was added to object type JourneyLog
✔  Input field JourneyLogInput.bags description changed from Bags to Bags [mass]
✔  Input field JourneyLogInput.pax description changed from PAX to PAX - a sum of Males, Females, Children
✔  Field Mutation.aircraftInsurance description changed from ' to Requires access to resource GRAPHQL_AIRCRAFT_INSURANCE_EDIT'
✔  Field landingPermits was added to object type MyLeg
✔  Field overflightPermits was added to object type MyLeg
✔  Field unit was added to object type OperatorSetting
✔  Type OperatorUnitSettingType was added
✔  Description Passenger input with contact and arrival departure passports. on type PassengerContactInput has changed to Passenger input with contact and arrival departure passports. NOTE isLead field is deprecated!
✔  Field PhonebookCompanyRepresentative.additionalCompanies description changed from ' to Requires access to resource GRAPHQL_CONTACT'
✔  Field PhonebookCompanyRepresentative.company description changed from ' to Requires access to resource GRAPHQL_CONTACT'
✔  Field VATAmount was added to object type Pricing
✔  Field VATRate was added to object type Pricing
✔  Field Query.aircraftInsurance description changed from ' to Requires access to resource GRAPHQL_AIRCRAFT_INSURANCE_SEE'
✔  Description for argument wildcard on field Query.passengerByWildcard changed from Minimum 1 characters length to undefined
✔  Field isChangedInOps was added to object type QuoteRealization
✔  Field subcharterFlightWatch was added to object type QuoteRealizationLeg
✔  Field changedInOpsRealizations was added to object type SalesMutation
✔  Field getQuoteRealizationLeg was added to object type SalesQuery
✔  Field aoc was added to object type Simulator
✔  Field aocSource was added to object type Simulator
✔  Type SubcharterFlightWatchInput was added
✔  Field docType was added to object type TravelDocumentsRecognition
Detected 21 breaking changes
