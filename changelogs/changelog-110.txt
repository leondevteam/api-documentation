Detected the following changes (97) between schemas:

✖  Field leg_end_user_nid was removed from object type JourneyLogHistoryEntry
✖  Input field addToCarbonCopy was added to input object type MailboxConfigInput
⚠  Input field dutyAccountDefinitionNid was added to input object type ApiDutyInput
⚠  Input field tbc was added to input object type ApiDutyInput
⚠  Enum value QUERY_VALIDITY_ERROR was added to enum CreateSubscriptionWebhookViolationEnum
⚠  Enum value TOKEN_EXPIRES_ERROR was added to enum CreateSubscriptionWebhookViolationEnum
⚠  Input field dutyAccountDefinitionNid was added to input object type DutyListEditInput
⚠  Input field tbc was added to input object type DutyListEditInput
⚠  Input field feedback was added to input object type JourneyLogInput
⚠  Enum value INTEGRATION_JETEX was added to enum PermissionResource
⚠  Input field emailWork2 was added to input object type PhonebookCompanyCreateInput
⚠  Input field emailWork2 was added to input object type PhonebookCompanyUpdateInput
⚠  Input field emailWork2 was added to input object type PhonebookPersonCreateInput
⚠  Input field emailWork2 was added to input object type PhonebookPersonUpdateInput
⚠  Input field emailWork2 was added to input object type PhonebookRepresentativeCreateInput
⚠  Input field emailWork2 was added to input object type PhonebookRepresentativeUpdateInput
⚠  Enum value BOOKED_OFFERED_OPERATOR_PRICE was added to enum QuotesSortMethod
⚠  Enum value OPERATOR_PRICE_ASC was added to enum QuotesSortMethod
⚠  Enum value OPERATOR_STATUS_OPERATOR_PRICE was added to enum QuotesSortMethod
⚠  Input field tripEditScheme was added to input object type RequestInputType
⚠  Input field cargo was added to input object type RfqItineraryInput
⚠  Input field cargoWeightUnit was added to input object type RfqItineraryInput
⚠  Input field addPositioning was added to input object type RfqPricingInput
⚠  Argument wildcard: String added to field crewList.crewMemberList
⚠  Argument wildcard: String added to field crewList.crewMemberRestrictedList
✔  Field city was added to object type Contact
✔  Field dutyAccount was added to object type CrewMember
✔  Field dutyAccountList was added to object type CrewMember
✔  Type CrewMemberDutyAccountInput was added
✔  Type CrewMemberDutyAccountMutationSection was added
✔  Type CrewMemberDutyAccountMutationSectionSetListOutput was added
✔  Type CrewMemberDutyAccountMutationSectionSetListViolation was added
✔  Type CrewMemberDutyAccountMutationSectionSetListViolationEnum was added
✔  Type CrewMemberDutyAccountMutationSectionSetListViolationList was added
✔  Type CrewMemberDutyAccountMutationSectionUpdateOutput was added
✔  Type CrewMemberDutyAccountMutationSectionUpdateViolation was added
✔  Type CrewMemberDutyAccountMutationSectionUpdateViolationEnum was added
✔  Type CrewMemberDutyAccountMutationSectionUpdateViolationList was added
✔  Field dutyAccount was added to object type CrewMemberMutationSection
✔  Field dutyAccountDefinitionNid was added to object type CrewPanelDuty
✔  Field tbc was added to object type CrewPanelDuty
✔  Field dutyDefinition was added to object type CrewPanelDutyType
✔  Field tbc was added to object type CrewPanelDutyType
✔  Type DutyAccount was added
✔  Field DutyDefinition.dutyDefinitionType has deprecation reason Use dutyDefinitionTypeEnum field
✔  Field DutyDefinition.dutyDefinitionType is deprecated
✔  Field dutyDefinitionTypeEnum was added to object type DutyDefinition
✔  Field custom_post_flight_duty_per_position was added to object type FtlSettings
✔  Field custom_reporting_time_per_position was added to object type FtlSettings
✔  Field local_nights_to_reset_time_zone_difference was added to object type FtlSettings
✔  Field min_diff_to_increase_rest_by_time_zone_difference was added to object type FtlSettings
✔  Field reduce_max_fdp_due_to_time_zone_difference was added to object type FtlSettings
✔  Field jetex was added to object type IntegrationMutation
✔  Field jetex was added to object type IntegrationQueries
✔  Type JetexFlight was added
✔  Type JetexFlightNid was added
✔  Type JetexFlightQuery was added
✔  Type JetexMutation was added
✔  Type JetexQuery was added
✔  Type JetexService was added
✔  Type JetexServiceEnum was added
✔  Type JetexServiceInput was added
✔  Type JetexServiceRequestInput was added
✔  Type JetexServiceStatus was added
✔  Type JetexSubscriptionSection was added
✔  Field JourneyLogHistoryEntry.changeTime changed type from DateTime to DateTime!
✔  Field feedback was added to object type JourneyLogHistoryEntry
✔  Field legEndUser was added to object type JourneyLogHistoryEntry
✔  Field addToCarbonCopy was added to object type MailboxConfig
✔  Field bloffLt was added to object type MyFlightWatch
✔  Field blonLt was added to object type MyFlightWatch
✔  Field etaLt was added to object type MyFlightWatch
✔  Field etdLt was added to object type MyFlightWatch
✔  Field ldgLt was added to object type MyFlightWatch
✔  Field toLt was added to object type MyFlightWatch
✔  Field adepSlotTime was added to object type MyLeg
✔  Field adepSlotTimeLT was added to object type MyLeg
✔  Field adesSlotTime was added to object type MyLeg
✔  Field adesSlotTimeLT was added to object type MyLeg
✔  Field departurePermitsItemStatus was added to object type MyLeg
✔  Type NonNullDutyAccountValue was added
✔  Type NonNullListOfNonNullDutyAccountValue was added
✔  Field trip was added to object type OperatorSetting
✔  Type OperatorTripSettings was added
✔  Field emailWork2 was added to object type PhonebookCompany
✔  Field emailWork2 was added to object type PhonebookCompanyRepresentative
✔  Field emailWork2 was added to object type PhonebookPerson
✔  Field cargo was added to object type QuoteRealizationLeg
✔  Field cargoWeightUnit was added to object type QuoteRealizationLeg
✔  Field tripEditScheme was added to object type QuoteRequest
✔  Field cargo was added to object type QuoteRequestLeg
✔  Field cargoWeightUnit was added to object type QuoteRequestLeg
✔  Field paxNumber was added to object type RfqQuoteLeg
✔  Field integration was added to object type Subscription
✔  Type TripEditScheme was added
✔  Description for argument refreshToken on field WebhookMutationSection.createSubscriptionWebhook changed from ' to Possible violation list: TOKEN_EXPIRES_ERROR'
✔  Description for argument subscription on field WebhookMutationSection.createSubscriptionWebhook changed from ' to Possible violation list: QUERY_VALIDITY_ERROR'
Detected 2 breaking changes
