#!/bin/bash

set -eux

cd "$(dirname $0)/.."

CHANGELOGS_DIR="./changelogs"
DOCKER_IMAGE="kamilkisiela/graphql-inspector@sha256:66727f60dc039b409856f7c1b0436fbe166e2259af581c314dbc55ce7c969bc2"
FORMAT_CMD="sed -re '/^\[([a-z]+)\] / s/\[([a-z]+)\] //' | tail -n+2"
SOURCE_URL="https://api-schema-doc.s3-eu-west-1.amazonaws.com/"

CURRENT_BETA_SPRINT="$(curl -s -I ${SOURCE_URL}schema-beta.json | tr -d '\r' | sed -rn 's/^x-amz-meta-sprint: ([0-9]+)$/\1/p')"
CURRENT_MASTER_SPRINT="$(curl -s -I ${SOURCE_URL}schema-master.json | tr -d '\r' | sed -rn 's/^x-amz-meta-sprint: ([0-9]+)$/\1/p')"
[ -n "$CURRENT_BETA_SPRINT" -a -n "$CURRENT_MASTER_SPRINT" ]
PRE_BETA_SPRINT="$(( $CURRENT_BETA_SPRINT - 1 ))"

curl -f -s "${SOURCE_URL}schema-beta.json" -o "schema-beta.json"
curl -f -s "${SOURCE_URL}schema-$PRE_BETA_SPRINT.json" -o "schema-$PRE_BETA_SPRINT.json"

mkdir -p $CHANGELOGS_DIR
docker pull -q $DOCKER_IMAGE
# https://graphql-inspector.com/docs/installation
(docker run -v $PWD:/app $DOCKER_IMAGE graphql-inspector diff "schema-$PRE_BETA_SPRINT.json" "schema-beta.json" | eval "$FORMAT_CMD" > "$CHANGELOGS_DIR/changelog-$CURRENT_BETA_SPRINT.txt") || true

# sometimes schema can change directly on master branch (usually this is caused by bugfixing)
# beta is merged to master after successful rolling deployment new (beta) version of code to production environment,
# usually at end of first week of each (2 weeks long) sprint,
# so often branch master == beta
# but change schema on master should be handled separately if beta isn't merged to master already
if [ $CURRENT_MASTER_SPRINT = $PRE_BETA_SPRINT ]; then
    PRE_MASTER_SPRINT="$(( $CURRENT_MASTER_SPRINT - 1 ))"
    curl -f -s "${SOURCE_URL}schema-$PRE_MASTER_SPRINT.json" -o "schema-$PRE_MASTER_SPRINT.json"
    (docker run -v $PWD:/app $DOCKER_IMAGE graphql-inspector diff "schema-$PRE_MASTER_SPRINT.json" "schema-$CURRENT_MASTER_SPRINT.json" | eval "$FORMAT_CMD" > "$CHANGELOGS_DIR/changelog-$CURRENT_MASTER_SPRINT.txt") || true
fi
if [ "$(git status --porcelain $CHANGELOGS_DIR/ | wc -l)" -gt 0 ]; then
  git add $CHANGELOGS_DIR
  git commit -m "[skip ci] GQL schema changelogs update, $(date -u)"
  git push
fi
