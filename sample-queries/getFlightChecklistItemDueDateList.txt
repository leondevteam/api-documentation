query {
  flights {
    getFlightChecklistItemDueDateList(
      from: "2024-04-05"
      limit: 100
      offset: 0
      wildcard: "ATC"
      type: ALL
      sortBy: REQUEST_TIME
    ) {
      ... on NonNullListOfNonNullFlightChecklistItemDueDateValue {
        value {
          checklistItem {
            cdNid
            csId
            definition {
              label
            }
          }
          confirmationDateTime
          flight {
            flightNid
          }
          requestDateTime
        }
      }
      ... on GetFlightChecklistItemDueDateListViolationList {
        value {
          category
          message
          path
        }
      }
    }
  }
}
