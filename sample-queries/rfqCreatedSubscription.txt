mutation {
  webhook {
    createSubscriptionWebhook(
      refreshToken: "<yourRefreshToken>",
      label: "NameOfSubscriptionThatCanBeUsedLaterToCancelIt",
      subscription: "subscription test {quoteRequest {rfqCreated { id operator { name oprId oprNid } legs { std adep { code { icao } } ades { code { icao } } sta paxNo distanceNM } }} }",
      variables: "{}",
      webhookUrl: "https://webhook_url/"
    ) {
      ... on CreateSubscriptionWebhookViolationList {
        value {
          message
          path
        }
      }
    }
  }
}