mutation {
  integration {
    flightSupport {
      flightPermits {
        deleteOverflightPermit(overflightPermitNid: 987654)
      }
    }
  }
}