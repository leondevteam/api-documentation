mutation {
  integration {
    flightSupport {
      setFlightWatch(
        flightNid: 12345678
        flightWatch: { bloffIso: { dateTime: "09:45" }, bloffFuel: 3500 }
      ) {
        flight {
          flightNid
          flightWatch {
            ... on FlightWatchValue {
              value {
                tobt
                ctotIso
                etdIso
                offBlock
                toIso
                eetIso
                etaIso
                ldgIso
                blonIso
              }
            }
            ... on ErrorList {
              errorList {
                message
                category
                path
              }
            }
          }
        }
      }
    }
  }
}
