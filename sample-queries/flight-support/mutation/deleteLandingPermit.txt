mutation {
  integration {
    flightSupport {
      flightPermits {
        deleteLandingPermit(landingPermitNid: 987654)
      }
    }
  }
}