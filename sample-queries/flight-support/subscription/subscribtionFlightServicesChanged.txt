mutation {
  webhook {
    createSubscriptionWebhook(
      refreshToken: "YOUR_REFRESH_TOKEN"
      label: "Sample flightServicesChanged"
      subscription: "subscription serviceChangeTest($operatorId: OperatorId!, $oAuthClientId: OAuthClientId!) { integration { flightSupport { flightServicesChanged( operatorId: $operatorId oAuthClientId: $oAuthClientId ) { flight { flightNid flightNo oprNid oprId tripNid tripNo isCnl aircraft { registration mtow weightUnit paxCapacity aircraftType { icao iata name } documentList { ... on NonNullListOfNonNullFlightSupportAircraftDocumentValue { value { name fileList { fileName signedUrl } } } ... on ErrorList { errorList { category message path } } } } startTimeUTC endTimeUTC departureAirport { code { icao iata available } } arrivalAirport { code { icao iata available } } previousFlight { flightNid departureDateTimeUTC arrivalDateTimeUTC departureAirport { code { icao iata } name } arrivalAirport { code { icao iata } name } } nextFlight { flightNid departureDateTimeUTC arrivalDateTimeUTC departureAirport { code { icao iata } name } arrivalAirport { code { icao iata } name } } icaoType slotAdep { ... on FlightSupportSlotChecklistItemValue { value { comment provider { name email } status { abbreviation caption status color } time } } ... on ErrorList { errorList { category message path } } } slotAdes { ... on FlightSupportSlotChecklistItemValue { value { comment provider { name email } status { abbreviation caption status color } time } } ... on ErrorList { errorList { category message path } } } handlingAdep { ... on FlightSupportChecklistItemValue { value { comment provider { name email } status { abbreviation caption status color } } } ... on ErrorList { errorList { category message path } } } handlingAdes { ... on FlightSupportChecklistItemValue { value { comment provider { name email } status { abbreviation caption status color } } } ... on ErrorList { errorList { category message path } } } fuelAdep { ... on FlightSupportFuelChecklistItemValue { value { comment status { abbreviation caption status color } blockFuel burnFuel minimumRequiredFuel requestedFuel tripFuel taxiFuel contingencyFuel fuelUnit } } ... on ErrorList { errorList { category message path } } } overflightPermits { ... on NonNullListOfNonNullLegPermitValue { value { legPermitNid number validity entryPoint exitPoint supplier notes status permitStatus revalidation shortNotice country { name code codeIso } permitOrder type files { createdOn fileId fileName signedUrl } } } ... on ErrorList { errorList { category message path } } } landingPermits { ... on NonNullListOfNonNullLegPermitValue { value { legPermitNid number validity entryPoint exitPoint supplier notes status permitStatus revalidation shortNotice country { name code codeIso } permitOrder type files { createdOn fileId fileName signedUrl } } } ... on ErrorList { errorList { category message path } } } flightWatch { ... on FlightWatchValue { value { flightWatchNid revisionId acft { registration } tobt ctot etd offBlock atd eta ata onBlock } } ... on ErrorList { errorList { category message path } } } crewCount crewList { position { posNid name positionType posType order occupation rawName positionGroup } name surname code arrivalTravelDocument { travelDocumentType number dateOfIssue dateOfExpiry country { code codeIso } nationality { code codeIso } name surname } departureTravelDocument { travelDocumentType number dateOfIssue dateOfExpiry country { code codeIso } nationality { code codeIso } name surname } dateOfBirth placeOfBirth gender nationality { code codeIso } licenseList { name fileList { fileName signedUrl } } } paxCount paxList { ... on NonNullListOfNonNullFlightSupportPassengerContactValue { value { name surname dateOfBirth gender arrivalTravelDocument { travelDocumentType number dateOfIssue dateOfExpiry name surname country { code codeIso } } } } ... on ErrorList { errorList { message category path } } } } isSharePaxDataEnabled serviceList { service note requested status fsNote } messages { id flightNid oauthClientNid type createdBy createdAt content isRead } } } } }"
      variables: "{\"operatorId\": \"OPR_ID\",\"oAuthClientId\": \"YOUR_OAUTH_CLIENT_ID\"}"
      webhookUrl: "https://YOUR_WEBHOOK_URL"
      includeAuthorizationHeader: true
    ) {
      ... on CreateSubscriptionWebhookViolationList {
        error: value {
          message
          path
          category
        }
      }
      ... on NonNullBooleanValue {
        success: value
      }
    }
  }
}
